const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const keys = require("../../config/keys");
const passport = require("passport");

// Load input validation
const validateLoginInput = require("../../validation/users/login");
const { validateData } = require("../../../common/functions");
const { Tables } = require("../../../common/schemas/schemas");
const { checkRights, hashString } = require("../../../common/functions");
const { saveUpdate } = require("./helpers");

// Load User model

router.delete(
  "/delete/:table",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    console.log(req);
    const table = req.params.table;
    if (!table) {
      res
        .status(500)
        .send({ global: "Define the table in API call delete/TABLE" });
      return;
    }
    const model = Tables[table].model;
    try {
      console.log(req.params, req.query);
      if (!checkRights("users", req.user, "edit")) {
        return res
          .status(400)
          .json({ global: "You dont have read permissions" });
      }
      if (!req.query.id) {
        return res.status(400).json({ global: "Id is not found" });
      }
      const results = await model.findByIdAndRemove(req.query.id);
      res.send(results);
    } catch (error) {
      console.log(error);
      res.status(500).send({ global: error.message || error });
    }
  }
);

router.get(
  "/get/:table",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    try {
      console.log("GETTING common", req.params, req.query);
      const table = req.params.table;
      if (!table) {
        res
          .status(500)
          .send({ global: "Define the table in API call delete/TABLE" });
        return;
      }
      if (!(table in Tables)) {
        res.status(500).send({ global: "Table not found" });
        return;
      }
      const model = Tables[table].model;
      if (!checkRights("users", req.user, "read")) {
        return res.status(400).json("You dont have read permissions");
      }
      const results = await (req.query.id
        ? model.findById(req.query.id).select({
            password: 0,
            password2: 0
          })
        : model.find()
      )
        .populate({
          path: "last_update",
          model: Tables.History.model,
          populate: {
            path: "updated_by",
            model: Tables.Users.model
          }
        })
        .exec();
      res.send(results);
    } catch (error) {
      console.log(error);
      res.status(500).send(error.message || error);
    }
  }
);

// @route POST api/users/update
// @desc update user
// @access Public

router.post(
  "/update/:table",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    console.log(req.params, req.query);
    const table = req.params.table;
    if (!table) {
      res
        .status(500)
        .send({ global: "Define the table in API call delete/TABLE" });
      return;
    }
    const model = Tables[table].model;
    // Form validation
    const { errors, isValid, res_data } = await validateData(
      req.body,
      Tables[table],
      "update",
      req.user,
      true
    );
    console.log(errors, isValid);
    // Check validation
    if (!isValid) {
      return res.status(400).json(errors);
    }
    const item = await model
      .findById(req.body._id)
      .catch(err => res.status(400).json("User not found"));
    saveUpdate(item, res_data, res, req, table);
  }
);

// @route POST api/users/register
// @desc Register user
// @access Public
router.post(
  "/post/:table",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    console.log(req.params, req.query);
    const table = req.params.table;
    if (!table) {
      return res
        .status(500)
        .send({ global: "Define the table in API call delete/TABLE" });
    }
    const model = Tables[table].model;
    const schema = Tables[table].schema;
    const { body, user } = req;
    // Form validation
    const { errors, isValid, new_data } = await validateData(
      body,
      Tables[table],
      "post",
      user
    );
    console.log(errors, isValid);
    if (!isValid) {
      return res.status(400).json(errors);
    }
    console.log(new_data);
    const new_record = new model(new_data);
    //TODO  Hash password before saving in database
    new_record
      .save()
      .then(user => res.json(new_data))
      .catch(err => console.log(err));
  }
);

module.exports = router;
