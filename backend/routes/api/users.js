const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const keys = require("../../config/keys");
const passport = require("passport");

// Load input validation
const validateLoginInput = require("../../validation/users/login");
const { validateData } = require("../../../common/functions");
const { Tables } = require("../../../common/schemas/schemas");
const { checkRights } = require("../../../common/functions");
const { saveUpdate } = require("./helpers");

// Load User model

const UsersModel = Tables.Users.model;

console.log(UsersModel);

router.delete(
  "/delete/:id?",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    console.log(req);
    try {
      console.log(req.params, req.query);
      if (!checkRights("users", req.user, "edit")) {
        return res
          .status(400)
          .json({ global: "You dont have read permissions" });
      }
      if (!req.query.id) {
        return res.status(400).json({ global: "Id is not found" });
      }
      const results = await UsersModel.findByIdAndRemove(req.query.id);
      res.send(results);
    } catch (error) {
      console.log(error);
      res.status(500).send({ global: error.message || error });
    }
  }
);

router.get(
  "/get/:id?",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    try {
      console.log(req.params, req.query);
      if (!checkRights("users", req.user, "read")) {
        return res.status(400).json("You dont have read permissions");
      }
      const results = await (req.query.id
        ? UsersModel.findById(req.query.id).select({
            password: 0,
            password2: 0
          })
        : UsersModel.find().select({ password: 0, password2: 0 })
      )
        .populate({
          path: "last_update",
          model: Tables.History.model,
          populate: {
            path: "updated_by",
            model: Tables.Users.model
          }
        })
        .exec();
      res.send(results);
    } catch (error) {
      console.log(error);
      res.status(500).send(error.message || error);
    }
  }
);

// @route POST api/users/update
// @desc update user
// @access Public

router.post(
  "/update",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    // Form validation
    const { errors, isValid } = await validateData(
      req.body,
      Tables.Users,
      "update",
      req.user,
      true
    );
    console.log(errors, isValid);
    // Check validation
    if (!isValid) {
      return res.status(400).json(errors);
    }
    const user = await UsersModel.findById(req.body._id).catch(err =>
      res.status(400).json("User not found")
    );

    if (req.body.email && user.email !== req.body.emai) {
      const existed_user = await UsersModel.findOne({ email: req.body.email });
      if (existed_user) {
        res.status(400).json("This mail is already taken");
      }
    }
    if (req.body.password) {
      const salt = await bcrypt.genSalt(10);
      const hash = await bcrypt
        .hash(req.body.password, salt)
        .catch(e => res.status(400).json(err));
      req.body.password = hash;
    }
    saveUpdate(user, req.body, res, req, "Users");
  }
);

// @route POST api/users/register
// @desc Register user
// @access Public
router.post(
  "/register",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    const { errors, isValid, data } = await validateData(
      req.body,
      Tables.Users,
      "post",
      req.user,
      true
    );
    console.log(errors, isValid);
    // Check validation
    if (!isValid) {
      return res.status(400).json(errors);
    }

    const newUser = new UsersModel({
      name: req.body.name,
      email: req.body.email,
      password: req.body.password,
      password2: req.body.password2,
      priority: req.body.priority,
      avatar: req.body.avatarupup
    });
    // Hash password before saving in database
    const salt = await bcrypt
      .genSalt(10)
      .catch(e => res.status(400).json({ global: e }));
    newUser.password = await bcrypt
      .hash(newUser.password, salt)
      .catch(e => res.status(400).json({ global: e }));
    newUser
      .save()
      .then(user => res.json(user))
      .catch(err => console.log(err));
  }
);

// @route POST api/users/login
// @desc Login user and return JWT token
// @access Public
router.post("/login", (req, res) => {
  // Form validation
  const { errors, isValid } = validateLoginInput(req.body);
  console.log(errors, req.body);
  // Check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }
  const email = req.body.email;
  const password = req.body.password;
  // Find user by email
  UsersModel.findOne({ email }).then(user => {
    // Check if user exists
    if (!user) {
      return res.status(404).json({ email: "Email not found" });
    }
    // Check password
    bcrypt.compare(password, user.password).then(isMatch => {
      if (isMatch) {
        // User matched
        // Create JWT Payload
        const { priority, avatar, email, name, id } = user;
        const payload = { id, priority, name, email, avatar };
        // Sign token
        jwt.sign(
          payload,
          keys.secretOrKey,
          {
            expiresIn: 31556926 // 1 year in seconds
          },
          (err, token) => {
            res.json({
              success: true,
              token: "Bearer " + token
            });
          }
        );
      } else {
        return res.status(400).json({ password: "Password incorrect" });
      }
    });
  });
});

module.exports = router;
