const { Tables } = require("../../../common/schemas/schemas");
const HistoryModel = Tables.History.model;

function removeEmpty(obj) {
  const keys = Object.keys(obj);
  const ret_obj = {};
  keys.forEach(key => {
    if (!(obj[key] === undefined || obj[key] === "")) ret_obj[key] = obj[key];
  });
  return ret_obj;
}

function detectChanges(old_obj, new_obj) {
  const keys = Object.keys(new_obj);
  const ret_obj_new = {};
  const ret_obj_old = {};

  keys.forEach(key => {
    if (!key.startsWith("_") && new_obj[key] !== old_obj[key]) {
      ret_obj_old[key] = old_obj[key];
      ret_obj_new[key] = new_obj[key];
    }
  });
  return { ret_obj_old, ret_obj_new };
}

module.exports = {
  saveUpdate: async function(query, new_data, res, req, table) {
    new_data = removeEmpty(new_data);
    const { ret_obj_old, ret_obj_new } = detectChanges(query, new_data);

    const keys = Object.keys(new_data);
    keys.forEach(key => {
      if (!(new_data[key] === undefined || new_data[key] === ""))
        query[key] = new_data[key];
    });
    const history = new HistoryModel({
      date: new Date().toISOString(),
      table,
      updated_by: req.user._id,
      previus_state: JSON.stringify(ret_obj_old),
      new_state: JSON.stringify(ret_obj_new)
    });
    console.log(history);

    query["last_update"] = history._id;
    return query
      .save()
      .then(e => {
        (history["updated_element"] = e._id),
          history
            .save()
            .then(h => res.json(h))
            .catch(e => {
              console.log("ERRRORRRRRRRR");
              console.log(e);
              res.status(400).json(e);
            });
      })
      .catch(e => {
        console.log("ERRRORRRRRRRR");
        console.log(e);
        res.status(400).json({ global: e });
      });
  }
};
