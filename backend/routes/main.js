const express = require("express");
const router = express.Router();
const passport = require("passport");

// @route POST api/users/register
// @desc Register user
// @access Public
router.get("/login", function(req, res) {
  res.redirect("/");
});

router.get("/", (req, res) => {
  const file = "index.html";
  //console.log(`Serving ${file}`);
  res.sendFile(file);
});

router.get(
  "/dashboard",
  passport.authenticate("jwt", {
    session: false,
    successRedirect: "/",
    failureRedirect: "/login"
  })
);

router.get(
  "/register",
  passport.authenticate("jwt", {
    session: false,
    failureRedirect: "/login"
  }),
  function(req, res) {
    //console.log(req.user);
    if (req.user.priority === "manager" || req.user.priority === "admin") {
      res.redirect("/register");
    } else {
      res.redirect("/dashboard");
    }
  }
);

module.exports = router;
