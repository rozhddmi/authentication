const LocalizedStrings = require("localized-strings").default;
//console.log(LocalizedStrings);

const LOCALIZED_STRING = require("./../../common/localization");

const localizator = new LocalizedStrings(LOCALIZED_STRING);
module.exports = localizator;
