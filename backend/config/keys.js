module.exports = {
  mongoURI: process.env.MONGO_SERVER + "/" + process.env.CARSTORE_DATABASE,
  mongoSettings: {
    dbName: process.env.CARSTORE_DATABASE,
    user: process.env.CARSTORE_ADMIN_USERNAME,
    pass: process.env.CARSTORE_ADMIN_PASSWORD,
    useNewUrlParser: true
  },
  mongoConnectionAttempts: 5,
  mongoConnectionTimeout: 5000,
  secretOrKey: process.env.SECRET,
  staticFolder: process.env.STATIC_PATH,
  appPort: process.env.PORT
};
