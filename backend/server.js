const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const passport = require("passport");
const users = require("./routes/api/users");
const common = require("./routes/api/common");
const main = require("./routes/main");
const path = require("path");
// Bodyparser middleware

const app = express();

const {
  mongoURI,
  mongoSettings,
  staticFolder,
  mongoConnectionAttempts,
  mongoConnectionTimeout,
  appPort
} = require("./config/keys");

var attempt = 0;

const connectWithRetry = () => {
  mongoose
    .connect(mongoURI, mongoSettings)
    .then(() => {
      console.log(`MongoDB successfully connected to ${mongoURI}`);
    })
    .catch(err => {
      console.log(
        `ERROR -----> MongoDB FAILED to connected to ${mongoURI} [${attempt}] of [${mongoConnectionAttempts}]`
      );
      if (attempt < mongoConnectionAttempts) {
        setTimeout(connectWithRetry, mongoConnectionTimeout);
        attempt++;
      } else {
        //console.log("ERROR -----> I Give up no connection to database");
        //console.log(mongoSettings);
        //console.log(err);
      }
    });
};

const statc = path.join(__dirname, staticFolder);
app.use(express.static(statc));

app.use(
  bodyParser.urlencoded({
    extended: false
  })
);
app.use(bodyParser.json());
// DB Config

// Connect to MongoDB
connectWithRetry();

// Passport middleware
app.use(passport.initialize());
// Passport config
require("./config/passport")(passport);
// Routes

app.use("/api/users", users);
//app.use("/api/cars", cars);
app.use("/api", common);

app.use("/", main);

app.listen(appPort, () =>
  console.log(`Server up and running on port ${appPort} !`)
);
