const Validator = require("validator");
const isEmpty = require("is-empty");
const { USER_RIGHTS } = require("./access_levels");

function checkRights(table, user, action, field) {
  user = USER_RIGHTS.fromString(user ? user.priority : "unathorized");
  if (!table) {
    //console.log("No table");
    return false;
  }
  if (!table.rights) {
    //console.log("No rights");
    return true;
  }
  if (!field) {
    //console.log("No fields");
    return table.rights[action] <= user;
  } else {
    const tb_field = table.schema[field];
    if (!tb_field) {
      return false;
    } else if (!tb_field.rights || tb_field.rights[action] === undefined) {
      return table.rights[action] <= user;
    } else {
      return tb_field.rights[action] <= user;
    }
  }
}

async function isFieldValid(data, table, field, method, user) {
  const { model, schema } = table;
  //console.log("isFieldValid", field, schema[field]);
  if (!checkRights(table, user, method, field)) {
    return "lc-noPermission";
  }
  if (schema[field].validator && schema[field].validator[method]) {
    for (const vf of schema[field].validator[method]) {
      //console.log(vf);
      const res = await vf(data, model, field, method);
      if (res) {
        return res;
      }
    }
    return undefined;
  }
  //console.log("no validator for method ");
  return undefined;
}

async function validateData(
  data,
  table,
  method,
  user,
  ignore_undefined = false,
  remove_protected = false
) {
  const errors = {};
  const res_data = {};
  if (!checkRights(table, user, method)) {
    errors.global = "lc-noPermission";
  } else {
    for (const field of Object.keys(table.schema)) {
      if (ignore_undefined && data[field] === undefined) {
        continue;
      }
      const res = await isFieldValid(data, table, field, method, user);
      if (res) {
        errors[field] = res;
      } else {
        if (
          table.schema[field].renderer &&
          table.schema[field].renderer.password_field
        ) {
          const hashed = hashString(data[field]);
          if (hashed.errors.length > 0) {
            errors[field] = hashed.errors.join(";");
          } else {
            res_data[field] = hashed.value;
          }
        } else {
          res_data[field] = data[field];
        }
      }
    }
  }
  //console.log(errors);
  return { isValid: isEmpty(errors), errors, res_data };
}

async function isUnique(data, model, field, method) {
  //console.log("isUnique", data, field, method);
  const resp = await model.findOne({ [field]: data[field] });
  if (resp && !data._id) {
    return "lc-notUnique";
  } else if (resp && data._id && resp._id === data._id) {
    return "lc-notUnique";
  }
  return undefined;
}

async function notEmpty(data, model, field, method) {
  //console.log("notEmpty", data, model, field, method);
  if (Validator.isEmpty(data[field] || "")) {
    return "ls-isEmpty";
  }
  return undefined;
}

async function isStrongPassword(data, model, field, method) {
  //console.log("isStrongPassword", data, model, field, method);

  if (data[field].length < 8) {
    return "ls-notStrongPassword";
  }
}

async function isEmail(data, model, field, method) {
  //"isEmail", data, model, field, method);
  if (!Validator.isEmail(data[field])) {
    return "ls-notEmail";
  }
  return undefined;
}

async function graterThenZero(data, model, field, method) {
  //"isEmail", data, model, field, method);
  if (data[field] <= 0) {
    return "lc-shouldBePositiv";
  }
  return undefined;
}

async function isDate(data, model, field, method) {
  const date = Validator.toDate(data[field]);
  if (date === null) {
    return "lc-shouldBeDate";
  }
  return undefined;
}

async function hashString(str) {
  const errors = [];
  const salt = await bcrypt.genSalt(10).catch(e => errors.push(e));
  const hash_str =
    salt && (await bcrypt.hash(str, salt).catch(e => errors.push(e)));
  return { value: hash_str, error: errors };
}
module.exports.hashString = hashString;
module.exports.isFieldValid = isFieldValid;
module.exports.checkRights = checkRights;
module.exports.validateData = validateData;
module.exports.isEmail = isEmail;
module.exports.isStrongPassword = isStrongPassword;
module.exports.notEmpty = notEmpty;
module.exports.isUnique = isUnique;
module.exports.graterThenZero = graterThenZero;
module.exports.isDate = isDate;
