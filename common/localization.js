const LOCALIZED_STRING = {
  en: {
    "ls-email": "Email",
    cars: "Vehicles",
    contactManager: "Contact your manager to get an access",
    customers: "Costomers",
    dateOfRegistratoion: "Date of registration",
    delete: "Delete",
    documents: "Document",
    edit: "Edit",
    email: "Email",
    enterFullName: "Enter full name",
    enterMail: "Enter email adress",
    enterPassword: "Enter Password",
    enterPasswordSixCharLong: "Enter your password at least 6 characters",
    errorOccured: "Error occured",
    goHome: "Go Home",
    hide: "Hide",
    hidePassword: "Hide password",
    id: "Id",
    logIn: "Log In",
    loginForm: "Login Form",
    logOut: "Log Out",
    name: "Name",
    noRightsToRegister: "You have no rights to do this, contact your manager",
    password: "Password",
    register: "Register",
    registrationForm: "Registeration Form",
    registerUser: "RegisterUser",
    repeatePassword: "Repeate the password",
    required: "(required)",
    seConfirmPassworRequired: "Confirm password is required",
    seEmailIsInvalid: "Invalid email adress",
    seEmailIsRequired: "Email is required",
    seEmailNotFound: "Email not found",
    seNoPermission: "You do not have write permissions",
    sePasswordIsRequired: "Passwor is required",
    sePasswordSixChar: "Password must be at least 6 characters",
    sePassworIncorrect: "Password is incorect",
    sePaswordMustMatch: "Password does not match",
    show: "Show",
    showPassword: "Show password",
    templates: "Templates",
    userAccessRights: "User access rights",
    userNotFound: "User not found",
    userRegistred: "User {0} registerd with {1} rights",
    users: "Users"
  },
  cz: {
    "ls-email": "Posta"
  }
};

module.exports = LOCALIZED_STRING;
