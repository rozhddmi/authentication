// Create Schema
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const History = {
  date: {
    type: Date,
    default: Date.now,
    renderer: {
      label: "ls-dateOfChange"
    }
  },
  table: {
    type: String,
    renderer: {
      label: "ls-table"
    }
  },
  updated_element: {
    type: Schema.Types.ObjectId,
    renderer: {
      label: "ls-updatedElemnt",
      key: "identificator"
    }
  },
  updated_by: {
    type: Schema.Types.ObjectId,
    ref: "Users",
    renderer: {
      label: "ls-userModified",
      key: "email"
    }
  },
  previus_state: {
    type: String,
    renderer: {
      label: "ls-previousState"
    }
  },
  new_state: {
    type: String,
    renderer: {
      label: "ls-newState"
    }
  }
};

module.exports = History;
