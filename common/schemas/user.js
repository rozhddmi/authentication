// Create Schema
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const { USER_RIGHTS } = require("../access_levels");

const {
  isEmail,
  isUnique,
  isStrongPassword,
  notEmpty
} = require("../functions");

const Users = {
  name: {
    type: String,
    required: true,
    renderer: {
      labe: "ls-name",
      help: "ls-enterFullName"
    },
    validator: {
      post: [notEmpty],
      update: [notEmpty]
    }
  },
  email: {
    type: String,
    required: true,
    renderer: {
      email_field: true,
      label: "ls-email",
      help: "ls-enterMail"
    },
    rights: {
      post: USER_RIGHTS.UNAUTHORIZED,
      update: USER_RIGHTS.PROTECTED
    },
    validator: {
      post: [notEmpty, isUnique, isEmail],
      update: [notEmpty, isUnique, isEmail]
    }
  },
  password: {
    type: String,
    required: true,
    renderer: {
      password_field: true,
      label: "ls-password",
      help: "ls-enterPasswordSixCharLong"
    },
    rights: {
      post: USER_RIGHTS.UNAUTHORIZED,
      update: USER_RIGHTS.SAME_USER,
      get: USER_RIGHTS.PROTECTED
    },
    validator: {
      post: [
        notEmpty,
        isStrongPassword,
        (data, model, field, method) => {
          return data[field] === data.password2
            ? undefined
            : "ls-passwordMatch";
        }
      ],
      update: [
        notEmpty,
        isStrongPassword,
        (data, model, field, method) => {
          return data[field] === data.password2
            ? undefined
            : "ls-passwordMatch";
        }
      ]
    }
  },
  password2: {
    type: String,
    renderer: {
      password_field: true,
      label: "ls-password2",
      help: "ls-repeatePassword"
    },
    rights: {
      update: USER_RIGHTS.SAME_USER,
      get: USER_RIGHTS.PROTECTED
    },
    validator: {
      post: [
        notEmpty,
        isStrongPassword,
        (data, model, field, method) => {
          return data[field] === data.password ? undefined : "ls-passwordMatch";
        }
      ],
      update: [
        notEmpty,
        isStrongPassword,
        (data, model, field, method) => {
          return data[field] === data.password ? undefined : "ls-passwordMatch";
        }
      ]
    }
  },
  date: {
    type: Date,
    default: Date.now,
    renderer: {
      label: "ls-dateOfRegistratoion"
    },
    rights: {
      update: USER_RIGHTS.PROTECTED,
      post: USER_RIGHTS.PROTECTED
    }
  },
  priority: {
    type: String,
    enum: ["user", "manager", "admin"],
    default: "user",
    renderer: {
      label: "ls-rights",
      help: "userAccessRights"
    }
  },
  avatar: {
    type: String,
    default: "",
    renderer: {
      label: "ls-avataar",
      help: "userAccessRights"
    },
    rights: {
      update: USER_RIGHTS.PROTECTED,
      post: USER_RIGHTS.PROTECTED
    }
  },
  last_update: {
    type: Schema.Types.ObjectId,
    renderer: {
      label: "ls-lastUpdate",
      key: d => d.date,
      ref_tabe: "History"
    },
    rights: {
      update: USER_RIGHTS.PROTECTED
    }
  }
};

module.exports = Users;
