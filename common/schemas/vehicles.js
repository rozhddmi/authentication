// Create Schema
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const { USER_RIGHTS } = require("../access_levels");

const { isUnique, notEmpty, graterThenZero, isDate } = require("../functions");

const Vehicles = {
  license_plate: {
    type: String,
    required: true,
    renderer: {
      labe: "ls-licensePlate",
      help: "ls-licensePlateHelp"
    },
    validator: {
      post: [notEmpty, isUnique],
      update: [notEmpty, isUnique]
    }
  },
  vin: {
    type: String,
    required: true,
    renderer: {
      label: "ls-vin",
      help: "ls-vinHelp"
    },
    validator: {
      post: [notEmpty, isUnique],
      update: [notEmpty, isUnique]
    }
  },
  brand: {
    type: String, //TODO ObjectID
    required: true,
    renderer: {
      label: "ls-brand",
      help: "ls-brandHelp",
      key: d => d.brand,
      ref_tabe: "VehicleModels"
    },
    validator: {
      post: [notEmpty],
      update: [notEmpty]
    }
  },
  model: {
    type: String, //TODO ObjectID
    required: true,
    renderer: {
      label: "ls-model",
      help: "ls-modelHelp",
      key: d => d.model,
      ref_tabe: "VehicleModels"
    },
    validator: {
      post: [notEmpty],
      update: [notEmpty]
    }
  },
  color: {
    type: String,
    renderer: {
      label: "ls-color"
    }
  },
  reg_id: {
    type: String,
    required: true,
    renderer: {
      label: "ls-reg_id",
      help: "ls-regIdHelp"
    },
    validator: {
      post: [notEmpty, isUnique],
      update: [notEmpty, isUnique]
    }
  },
  engine_volume: {
    type: Number,
    default: 0,
    renderer: {
      label: "ls-engineVolume",
      help: "ls-engineVolumeHelp"
    },
    validator: {
      post: [notEmpty, graterThenZero],
      update: [notEmpty, graterThenZero]
    }
  },
  engine_power: {
    type: Number,
    default: 0,
    renderer: {
      label: "ls-enginePower",
      help: "ls-enginePowerHelp"
    },
    validator: {
      post: [notEmpty, graterThenZero],
      update: [notEmpty, graterThenZero]
    }
  },
  engine_power: {
    type: Number,
    default: 0,
    renderer: {
      label: "ls-enginePower",
      help: "ls-enginePowerHelp"
    },
    validator: {
      post: [notEmpty, graterThenZero],
      update: [notEmpty, graterThenZero]
    }
  },
  odometer: {
    type: Number,
    default: 0,
    renderer: {
      label: "ls-odometer",
      help: "ls-odometerHelp"
    },
    validator: {
      post: [notEmpty, graterThenZero],
      update: [notEmpty, graterThenZero]
    }
  },
  sale_status: {
    type: String,
    enum: ["ls-inStok", "ls-reserved", "ls-sold"],
    default: "ls-inStok",
    renderer: {
      label: "ls-status",
      help: "ls-statusHelp"
    }
  },
  registration_date: {
    type: Date,
    default: Date.now,
    renderer: {
      label: "ls-dateOfRegistratoion",
      help: "ls-dateOfRegistratoionHelp"
    },
    validator: {
      post: [notEmpty, isDate],
      update: [notEmpty, isDate]
    }
  },
  stock_date: {
    type: Date,
    default: Date.now,
    renderer: {
      label: "ls-stockDate",
      help: "ls-stockDateHelp"
    },
    validator: {
      post: [notEmpty, isDate],
      update: [notEmpty, isDate]
    },
    rights: {
      update: USER_RIGHTS.PROTECTED
    }
  },
  next_check: {
    type: Date,
    default: Date.now,
    renderer: {
      label: "ls-checkDate",
      help: "ls-checkDateHelp"
    },
    validator: {
      post: [notEmpty, isDate],
      update: [notEmpty, isDate]
    }
  },
  notes: {
    type: String,
    renderer: {
      label: "ls-note",
      help: "ls-noteHelp"
    }
  },
  internal_id: {
    type: String,
    renderer: {
      label: "ls-internalId",
      help: "ls-internalIdHelp"
    }
  },
  last_update: {
    type: Schema.Types.ObjectId,
    renderer: {
      label: "ls-lastUpdate",
      key: d => d.date,
      ref_tabe: "History"
    },
    rights: {
      update: USER_RIGHTS.PROTECTED
    }
  }
};

module.exports = Vehicles;

//license_plate - RZ -
//vin-  VIN -
//brand-  Značka -
//model -  Typ/Model -
//color-   Barva -
//reg_id- Číslo TP -
//engine_volume-  Objem motoru -
//engine_power-  Výkon motoru -
//odometer-  Stav km -
//sale_status-  Sta v prodeje (skladem, rezervace, prodáno) -

//internal_id -  Interní evideční číslo (pro potřeby klienta, není dále zpracovávano v dokumentech jen zobrazení na skladě) -

//next_check - Datum následující STK (pro potřeby hlídání blížící se následující kontroly) -
//registration_date -  Datum první registrace -

//stock_date 0 Datum naskladnění vozidla -
//note - Poznámka k vozidlu na skladě (pro účely spojené s e záznamem vozidl a, nikoli propoužití ve vytvářenýchdokumentech) - noted

//(prvni vstup rucni.... - prepocet na datum ) Počet dní do EK (evideční kontrola, ručně zadaný údaj odpočítávaný)
// [CALCULATED s predhoziho pole] Poočet dní do EK (evideční kontrola, ručně zadaný údaj odpočítávaný)
// [CALCULATED] Dne zbyva do nasledujici STK
// [CALCULATED] Doba na skladě udávana ve dnech

// [ HISTORY TABLE] Uživatel přidávajícínové vozidlo/vykonávající poslední úprav u stávajícího vozidla
// [ HISTORY TABLE] Datum poslední změny záznamu o vozidle
// Možná další pole pro interní použití dle potřeby
