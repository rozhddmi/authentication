const Users = require("./user");
const History = require("./history");
const Vehicles = require("./vehicles");

const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const { USER_RIGHTS } = require("../access_levels");

const Tables = Object.freeze({
  Users: {
    name: "ls-User",
    schema: Users,
    model: mongoose.model("Users", new Schema(Users)),
    rights: {
      post: USER_RIGHTS.MANAGER,
      get: USER_RIGHTS.SAME_USER,
      update: USER_RIGHTS.SAME_USER,
      delete: USER_RIGHTS.MANAGER,
      export: USER_RIGHTS.ADMIN,
      import: USER_RIGHTS.ADMIN
    }
  },
  Vehicles: {
    name: "ls-Vehicles",
    schema: Vehicles,
    model: mongoose.model("Vehicles", new Schema(Vehicles)),
    rights: {
      post: USER_RIGHTS.USER,
      get: USER_RIGHTS.USER,
      update: USER_RIGHTS.USER,
      delete: USER_RIGHTS.USER,
      export: USER_RIGHTS.MANAGER,
      import: USER_RIGHTS.MANAGER
    }
  },
  History: {
    name: "ls-History",
    schema: History,
    model: mongoose.model("History", new Schema(History)),
    rights: {
      post: USER_RIGHTS.PROTECTED,
      get: USER_RIGHTS.MANAGER,
      update: USER_RIGHTS.PROTECTED,
      delete: USER_RIGHTS.PROTECTED,
      export: USER_RIGHTS.ADMIN,
      import: USER_RIGHTS.ADMIN
    }
  }
});

module.exports.Tables = Tables;
