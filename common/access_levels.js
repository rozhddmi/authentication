module.exports.USER_RIGHTS = Object.freeze({
  PROTECTED: 1000,
  ADMIN: 4,
  MANAGER: 3,
  SAME_USER: 2,
  USER: 1,
  UNAUTHORIZED: 0,
  fromString: function(user) {
    switch (user) {
      case "admin":
        return this.ADMIN;
      case "user":
        return this.USER;
      case "manager":
        return this.MANAGER;
      case "unauthorized":
        return this.UNAUTHORIZED;
      default:
        return 0;
    }
  }
});
