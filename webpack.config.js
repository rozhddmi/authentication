const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const $$__PROD__$$ = process.argv.indexOf("-p") >= 0;
console.log("IS PRODUCTION", $$__PROD__$$);

const SRC = path.join(__dirname, "./frontend");
const DST = path.join(__dirname, "./dist/frontend");

module.exports = {
  entry: {
    vendor: [
      // Required to support async/await
      "@babel/polyfill"
    ],
    main: path.join(SRC, "./main.tsx")
  },
  output: {
    path: DST,
    filename: "[name].js"
  },
  devtool: false,
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".jsx"],
    mainFields: ["module", "browser", "main"],
    alias: {
      mobx: path.resolve(__dirname, "node_modules/mobx"),
      react: path.resolve(path.join(__dirname, "./node_modules/react")),
      "react-dom": "@hot-loader/react-dom"
    }
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            cacheDirectory: true,
            babelrc: false,
            presets: [
              [
                "@babel/preset-env",
                { targets: { browsers: "last 2 versions" } } // or whatever your project requires
              ],
              "@babel/preset-typescript",
              "@babel/preset-react"
            ],
            plugins: [
              // plugin-proposal-decorators is only needed if you're using experimental decorators in TypeScript
              ["@babel/plugin-proposal-decorators", { legacy: true }],
              ["@babel/plugin-proposal-class-properties", { loose: true }],
              "react-hot-loader/babel"
            ]
          }
        }
      },
      // For production extract css to files, so they can load in parallel to js
      {
        test: /\.css$/,
        use: [$$__PROD__$$ ? "style-loader" : "style-loader", "css-loader"]
      },
      // Keep original filename of jpgs, png and gif images
      { test: /\.(jpe?g|png|gif)$/, loader: "file-loader?name=[name].[ext]" },
      // Add a hash to fonts and svgs (which are probably fonts too)
      {
        test: /\.(woff2?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        loader: "file-loader?name=[name]_[hash].[ext]"
      }
    ]
  },
  plugins: [
    new ForkTsCheckerWebpackPlugin(),
    new webpack.NamedModulesPlugin(),
    new HtmlWebpackPlugin({
      template: path.join(SRC, "/assets/index.html")
    }),
    new webpack.DefinePlugin({
      PRODUCTION: JSON.stringify($$__PROD__$$),
      VERSION: JSON.stringify(require("./package.json").version)
    })
  ],
  devServer: {
    contentBase: SRC,
    hot: true,
    port: 3000,
    stats: "minimal",
    clientLogLevel: "warning",
    overlay: true,
    proxy: {
      "/api": "http://localhost:5000",
      "/dashboard": "http://localhost:5000",
      "/login": "http://localhost:5000",
      "/register": "http://localhost:5000"
    }
  },
  devtool: $$__PROD__$$ ? undefined : "cheap-module-source-map"
};
