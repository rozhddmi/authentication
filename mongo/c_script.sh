echo ======================================================
echo ${mongo[@]}  Creating default user "{$CARSTORE_ADMIN_USERNAME}" in collection users in  "{$CARSTORE_DATABASE}"
echo ======================================================

mcd="${mongo[@]} -u ${MONGO_INITDB_ROOT_USERNAME} -p ${MONGO_INITDB_ROOT_PASSWORD} --authenticationDatabase ${rootAuthDatabase} ${CARSTORE_DATABASE}"
echo $mcd

$mcd <<-EOJS
  db.createUser({
     user: $(_js_escape "$CARSTORE_ADMIN_USERNAME"),
     pwd: $(_js_escape "$CARSTORE_ADMIN_PASSWORD"),
     roles: [ { role: 'readWrite', db: $(_js_escape "$CARSTORE_DATABASE") } ]

     })
EOJS



echo ======================================================
echo created "$CARSTORE_ADMIN_USERNAME" in database "$CARSTORE_DATABASE" WITH PASSWORF "$CARSTORE_ADMIN_PASSWORD"
echo ======================================================
