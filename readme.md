# install docker

## First, update your existing list of packages:

```console
$ sudo apt update
```

### Next, install a few prerequisite packages which let apt use packages over HTTPS:

```console
$ sudo apt install apt-transport-https ca-certificates curl software-properties-common
```

### Then add the GPG key for the official Docker repository to your system:

```console
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

### Add the Docker repository to APT sources:

```console
$ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
```

### Next, update the package database with the Docker packages from the newly added repo:

```console
$ sudo apt update
```

##\$ Make sure you are about to install from the Docker repo instead of the default Ubuntu repo:

```console
$ apt-cache policy docker-ce

Output:
docker-ce:
  Installed: (none)
  Candidate: 18.03.1~ce~3-0~ubuntu
  Version table:
     18.03.1~ce~3-0~ubuntu 500
        500 https://download.docker.com/linux/ubuntu bionic/stable amd64 Packages
```

Notice that docker-ce is not installed, but the candidate for installation is from the Docker repository for Ubuntu 18.04 (bionic).

### Finally, install Docker blah:

```console
$ sudo apt install docker-ce
```

Docker should now be installed, the daemon started, and the process enabled to start on boot. Check that it’s running:

```console
$ sudo systemctl status docker

Output : docker.service - Docker Application Container Engine
   Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
   Active: active (running) since Thu 2018-07-05 15:08:39 UTC; 2min 55s ago
     Docs: https://docs.docker.com
 Main PID: 10096 (dockerd)
    Tasks: 16
   CGroup: /system.slice/docker.service
           ├─10096 /usr/bin/dockerd -H fd://
           └─10113 docker-containerd --config /var/run/docker/containerd/containerd.toml
```

Installing Docker now gives you not just the Docker service (daemon) but also the docker command line utility, or the Docker client. We’ll explore how to use the docker command later in this tutorial.

# Run Docker commands without sudo

Avoid typing sudo whenever you run the docker command,

### Add your username to the docker group:

```console
$ sudo usermod -aG docker ${USER}
```

### Apply the new group membership, log out of the server and back in, or type the following:

```console
$ su - ${USER}
```

You will be prompted to enter your user’s password to continue.

### Confirm that your user is now added to the docker group by typing:

```console
$ id -nG

Output: root sudo docker
```

If you need to add a user to the docker group that you’re not logged in as, declare that username explicitly using:

```console
$ sudo usermod -aG docker username
```

# Install docker compose

### Run this command to download the current stable release of Docker Compose:

```console
$ sudo curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

To install a different version of Compose, substitute 1.24.1 with the version of Compose you want to use.

If you have problems installing with curl, see Alternative Install Options tab above.

### Apply executable permissions to the binary:

```console
$ sudo chmod +x /usr/local/bin/docker-compose
```

Note: If the command docker-compose fails after installation, check your path. You can also create a symbolic link to /usr/bin or any other directory in your path.

For example:

```console
$ sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```

Optionally, install command completion for the bash and zsh shell.

### Test the installation.

```
$ docker-compose --version

Output: docker-compose version 1.24.1, build 1110ad01
```

# Clone the repo

```console
$ git clone https://gitlab.com/rozhddmi/authentication.git
User name and pasword of your gitlab
```

# Traefico setup

### Opent port for teafic

```console
$ sugo apr-get install ufw
$ sudo ufw allow 80
$ sudo ufw allow 443
$ sudo ufw allow 27017
$ sudo ufw allow 27018
$ sudo ufw allow 27019
```

### Create acme.json

```console
$ mkdir acme.json
$chmod -R 600 acme.json
```

### acces to the admn panel

username : admin
password : monkeyballs

# Start the app

We are creating separate physical volumes for mongodb, this way we always
can get access to them without interacting with docker container

````console
$ ocker volume create mongoconfig
$ docker volume create mongo_data

Note: DO NOTE PRUNE DOCKER VOLUMES  othervise data is lost

```console
$ docker-compose build
$ docker-compose up -d
````

### Rebuild and restart the carstore service without killing traefiko

```console
$ docker-compose up -d --force-recreate --build carstore
```

### Kill them all

```console
$ docker-compose down
```
