import * as React from "react";
import { Redirect, Route, RouteProps } from "react-router";
import { observer } from "mobx-react";
import { MainContainer } from "../stores";

@observer
export class ProtectedRoute extends Route<RouteProps> {
  public render() {
    let redirectPath: string = "";
    if (!MainContainer.registerStore.is_authenticated) {
      redirectPath = MainContainer.registerStore.authentication_path;
    }

    if (redirectPath) {
      const renderComponent = () => (
        <Redirect to={{ pathname: redirectPath }} />
      );
      return (
        <Route {...this.props} component={renderComponent} render={undefined} />
      );
    } else {
      return <Route {...this.props} />;
    }
  }
}
