import * as React from "react";
import { observer } from "mobx-react";

@observer
export class Landing extends React.Component<{}, {}> {
  render() {
    return <div style={{ width: "100vw", height: "calc(100vh - 50px)" }}></div>;
  }
}
