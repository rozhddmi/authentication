import React from "react";
import { hot } from "react-hot-loader/root";

import { observer } from "mobx-react";
import NavigationBar from "./navbar";
import Dashboard from "./dashboard";

import { Landing } from "./landing";

import { Router, Route, Switch } from "react-router-dom";
import jwt_decode from "jwt-decode";
import { ProtectedRoute } from "./private_route";

import { IUser } from ".";
import { MainContainer } from "../stores";

// Check for token to keep user logged in

@observer
class App extends React.Component<{}, {}> {
  constructor(props) {
    super(props);
    const { registerStore } = MainContainer;
    if (localStorage.jwtToken) {
      // Set auth token header auth
      const token = localStorage.jwtToken;
      registerStore.setAuthToken(token, false);
      // Decode token and get user info and exp
      const decoded = jwt_decode<IUser | undefined>(token);
      // Set user and isAuthenticated
      const currentTime = Date.now() / 1000; // to get in milliseconds
      if ((decoded as any).exp < currentTime) {
        // Logout user
        registerStore.logoutUser();
      }
    }
  }

  render() {
    return (
      <Router history={MainContainer.history}>
        <NavigationBar />
        <Route exact path="/" component={Landing} />
        <Switch>
          <ProtectedRoute path="/dashboard" component={Dashboard} />
        </Switch>
      </Router>
    );
  }
}

export default hot(App);
