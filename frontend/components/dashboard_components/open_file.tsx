import * as React from "react";

import { observer } from "mobx-react";
import { Button } from "@blueprintjs/core";
import { MainContainer } from "../../stores";
import { observable } from "mobx";
import { BigFileLoader } from "../../typescript/file_loader";

interface IImportTable {
  onFileReady: { (data: string): void };
  fileType?: string; //".json"
}

@observer
export class ReadFileButton extends React.Component<IImportTable, {}> {
  myRef = React.createRef<HTMLInputElement>();
  @observable loading_state: string | undefined = undefined;
  /**
   * Calling a file select windows dialog by means of hidden input field
   * in future make a appropriate dialog
   */
  private _onFileOpenClick = (): void => {
    if (this.myRef.current) {
      this.myRef.current.click();
    }
  };

  /**
   * Actual file load function fired when input field has changed
   */
  private _onFileSelected = (
    event: React.ChangeEvent<HTMLInputElement>
  ): void => {
    const { localize } = MainContainer.appStore;
    if (event.target.value !== null && event.target && event.target.files) {
      const file = event.target.files[0];
      const reader = new BigFileLoader();
      reader.onprogress = (content: any, progress: number) => {
        this.loading_state = `${localize("ls-Loading")}: ${(
          progress * 100
        ).toFixed(1)}%`;
      };
      reader.onload = (contents: any) => {
        this.loading_state = `Loaded`;
        setTimeout(() => {
          try {
            this.props.onFileReady(contents);
            this.loading_state = localize("ls-Loaded");
          } catch (e) {
            console.log(e);
            this.loading_state = localize("ls-FailedToLoad");
          }
        }, 100);
      };
      reader.onerror = e => {
        console.log(e);
        this.loading_state = localize("ls-FailedToLoad");
      };
      reader.onloadstart = (ev: Event) => {
        this.loading_state = localize(
          `${localize("ls-Loading")}: ${file.name}`
        );
      };

      reader.readAsText(file);
    }
    event.target.value = "";
  };

  render() {
    const { localize } = MainContainer.appStore;
    const { fileType } = this.props;
    return (
      <>
        <Button
          className="fixed_size"
          icon="folder-open"
          text={localize("ls-OpenFile")}
          onClick={this._onFileOpenClick}
        />
        <input
          style={{ visibility: "hidden", width: "0px" }}
          type="file"
          ref={this.myRef}
          onChange={this._onFileSelected}
          accept={fileType}
        />
      </>
    );
  }
}
