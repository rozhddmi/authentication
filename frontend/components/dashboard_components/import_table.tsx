import * as React from "react";
import {
  Cell,
  Column,
  Table,
  ColumnHeaderCell,
  Utils
} from "@blueprintjs/table";

import {
  Button,
  Tooltip,
  Dialog,
  Classes,
  Menu,
  MenuItem
} from "@blueprintjs/core";
import { observer } from "mobx-react";
import { observable, action, computed } from "mobx";
import { ShemaRenderer } from "..";
import { MainContainer } from "../../stores";
import CSV from "csvtojson";
import "@blueprintjs/table/lib/css/table.css";
import { ReadFileButton } from "./open_file";

export type ICellLookup = (rowIndex: number, columnIndex: number) => any;
export type ISortCallback = (
  columnIndex: number,
  comparator: (a: any, b: any) => number
) => void;

export interface ISortableColumn {
  getColumn(getCellData: ICellLookup, sortColumn: ISortCallback): JSX.Element;
}

abstract class AbstractSortableColumn implements ISortableColumn {
  constructor(protected name: string, protected index: number) {}

  public getColumn(getCellData: ICellLookup, sortColumn: ISortCallback) {
    const cellRenderer = (rowIndex: number, columnIndex: number) => (
      <Cell>{getCellData(rowIndex, columnIndex)}</Cell>
    );
    const menuRenderer = this.renderMenu.bind(this, sortColumn);
    const columnHeaderCellRenderer = () => (
      <ColumnHeaderCell name={this.name} menuRenderer={menuRenderer} />
    );
    return (
      <Column
        cellRenderer={cellRenderer}
        columnHeaderCellRenderer={columnHeaderCellRenderer}
        key={this.index}
        name={this.name}
      />
    );
  }

  protected abstract renderMenu(sortColumn: ISortCallback): JSX.Element;
}

class TextSortableColumn extends AbstractSortableColumn {
  protected renderMenu(sortColumn: ISortCallback) {
    const sortAsc = () => sortColumn(this.index, (a, b) => this.compare(a, b));
    const sortDesc = () => sortColumn(this.index, (a, b) => this.compare(b, a));
    return (
      <Menu>
        <MenuItem icon="sort-asc" onClick={sortAsc} text="Sort Asc" />
        <MenuItem icon="sort-desc" onClick={sortDesc} text="Sort Desc" />
      </Menu>
    );
  }

  private compare(a: any, b: any) {
    return a.toString().localeCompare(b);
  }
}

interface IImportTable {
  schema_store: ShemaRenderer;
}

@observer
export class ImportDialogButton extends React.Component<IImportTable, {}> {
  @observable private isOpen: boolean = false;
  @observable parsedData: Object[] = [];
  @observable sortedIndexMap: number[] = [];
  @computed get headerMap(): string[] {
    if (this.parsedData.length > 0) {
      return Object.keys(this.parsedData[0]);
    }
    return [];
  }

  @action
  private handleOpen = () => {
    this.isOpen = true;
  };

  @action
  private handleClose = () => {
    this.isOpen = false;
  };

  cellRenderer = (rowIndex: number) => {
    return <Cell>{`$${(rowIndex * 10).toFixed(2)}`}</Cell>;
  };

  @action
  readCsvString = (data: string) => {
    const convertor = CSV({}, {});
    convertor
      .fromString(data)
      .then(action((res: Object[]) => (this.parsedData = res)));
  };

  private getCellData = (rowIndex: number, columnIndex: number) => {
    const sortedRowIndex = this.sortedIndexMap[rowIndex];
    if (sortedRowIndex != null) {
      rowIndex = sortedRowIndex;
    }
    const column = this.headerMap[columnIndex];
    return this.parsedData[rowIndex][column];
  };

  @action.bound
  private sortColumn = (
    columnIndex: number,
    comparator: (a: any, b: any) => number
  ) => {
    const sortedIndexMap = Utils.times(
      this.parsedData.length,
      (i: number) => i
    );
    const column = this.headerMap[columnIndex];
    sortedIndexMap.sort((a: number, b: number) => {
      return comparator(this.parsedData[a][column], this.parsedData[b][column]);
    });
    this.sortedIndexMap = sortedIndexMap;
  };

  render() {
    const { localize } = MainContainer.appStore;
    const { table_name } = this.props.schema_store;
    const columns = this.parsedData
      .map((obj, i) => new TextSortableColumn(this.headerMap[i], i))
      .map(col => col.getColumn(this.getCellData, this.sortColumn));
    return (
      <>
        <Tooltip content={"ls-impportButtonText"}>
          <Button
            minimal
            icon="import"
            text={localize("ls-Import")}
            onClick={this.handleOpen}
          />
        </Tooltip>
        <Dialog
          isOpen={this.isOpen}
          onClose={this.handleClose}
          title={localize("ls-Import") + localize(table_name)}
        >
          <div className={Classes.DIALOG_BODY}>
            <Table numRows={this.parsedData.length}>{columns}</Table>
          </div>
          <div className={Classes.DIALOG_FOOTER}>
            <div className={Classes.DIALOG_FOOTER_ACTIONS}>
              <ReadFileButton
                onFileReady={this.readCsvString}
                fileType=".csv"
              />
            </div>
          </div>
        </Dialog>
      </>
    );
  }
}
