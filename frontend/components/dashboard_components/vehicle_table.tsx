import * as React from "react";
import { observer } from "mobx-react";
import { MainContainer } from "../../stores/index";

@observer
export class CarsTable extends React.Component<{}, {}> {
  render() {
    const { carTable, form_dialog } = MainContainer.carsStore;
    return (
      <div style={{ width: "100vw", height: "calc(100vh - 50px)" }}>
        {form_dialog}
        {carTable}
      </div>
    );
  }
}
