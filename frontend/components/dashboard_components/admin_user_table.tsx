import * as React from "react";
import { observer } from "mobx-react";
import { MainContainer } from "../../stores/index";

@observer
export class UserTable extends React.Component<{}, {}> {
  render() {
    const { userTable, form_dialog } = MainContainer.registerStore!;
    return (
      <div style={{ width: "100vw", height: "calc(100vh - 50px)" }}>
        {form_dialog}
        {userTable}
      </div>
    );
  }
}
