import * as React from "react";
import { observer } from "mobx-react";
import { MainContainer } from "../../stores/index";

@observer
export class HistoryTable extends React.Component<{}, {}> {
  render() {
    const { historyTable, form_dialog } = MainContainer.historyStore;
    return (
      <div style={{ width: "100vw", height: "calc(100vh - 50px)" }}>
        {form_dialog}
        {historyTable}
      </div>
    );
  }
}
