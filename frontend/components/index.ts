export * from "./dashboard";
export * from "./landing";
export * from "./navbar";
export * from "./dashboard_components";
export * from "./mongoose_renderer";
import App from "./app";

export default App;
