// import { hot } from "react-hot-loader/root";

import * as React from "react";
import { observer } from "mobx-react";
import { AuthentificationStore, MainContainer } from "../stores";
import { AppStore } from "../stores/app_store";
import { UserTable } from "./dashboard_components/admin_user_table";
import { Landing } from "./landing";
import { CarsTable } from "./dashboard_components/vehicle_table";
import { HistoryTable } from "./dashboard_components/history_table";

@observer
class Dashboard extends React.Component<
  {
    registerStore?: AuthentificationStore;
    appStore?: AppStore;
  },
  {}
> {
  render() {
    const { user } = MainContainer.registerStore!;
    const { curent_tab } = MainContainer.appStore!;
    let child: JSX.Element;
    switch (curent_tab) {
      case "Users":
        child = <UserTable />;
        break;
      case "Vehicles":
        child = <CarsTable />;
        break;
      case "History":
        child = <HistoryTable />;
        break;
      case "Documents":
      case "Customers":
      case "Templates":
      case "Home":
      default:
        child = user ? <Landing /> : <React.Fragment />;
        break;
    }
    return <>{child}</>;
  }
}

export default Dashboard;
