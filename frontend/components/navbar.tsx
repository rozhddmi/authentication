//import { hot } from "react-hot-loader/root";

import * as React from "react";
import { observer, inject } from "mobx-react";
import {
  Tooltip,
  Button,
  Navbar,
  Alignment,
  Tabs,
  Tab,
  Popover,
  ButtonGroup,
  HTMLSelect
} from "@blueprintjs/core";
import { AuthentificationStore, MainContainer } from "../stores/index";
import { ILangs, Langs } from "../typescript/typedefs";

const UserMenu = inject("registerStore")(
  (props: { registerStore?: AuthentificationStore }) => {
    return (
      <ButtonGroup vertical={true}>
        <Button
          minimal
          text="Log out"
          icon="log-out"
          onClick={props.registerStore!.logoutUser}
        />
        <Button minimal text="Settings" icon="settings" />
      </ButtonGroup>
    );
  }
);

@observer
class NavigationBar extends React.Component<{}, {}> {
  public constructor(props) {
    super(props);
  }

  render() {
    const { user, goToDashboard } = MainContainer.registerStore;
    const {
      curent_tab,
      avaliable_tabs,
      setCurrentTab,
      localize,
      setCurrentLang,
      curent_laguage
    } = MainContainer.appStore;
    const tabs: JSX.Element[] = [];
    avaliable_tabs.forEach(id => {
      tabs.push(<Tab key={"Tab" + id} id={id} title={localize(id)} />);
    });
    return (
      <Navbar>
        <Navbar.Group align={Alignment.LEFT}>
          <Navbar.Heading>
            {!user ? (
              MainContainer.registerStore.loginButton()
            ) : (
              <Popover content={<UserMenu />}>
                <Button minimal text={user.name} icon={"user"} />
              </Popover>
            )}
          </Navbar.Heading>
          <Navbar.Divider />

          <Tabs
            animate={true}
            id="navbar"
            large={true}
            onChange={setCurrentTab}
            selectedTabId={curent_tab}
          >
            {tabs}
          </Tabs>
        </Navbar.Group>
        <Navbar.Group align={Alignment.RIGHT}>
          <Navbar.Divider />

          <Tooltip content={localize("ls-goHome")}>
            <Button minimal icon="home" onClick={goToDashboard} />
          </Tooltip>
          <HTMLSelect
            value={curent_laguage}
            onChange={e => {
              setCurrentLang(e.currentTarget.value as ILangs);
            }}
          >
            {Langs.map(e => (
              <option key={e} value={e}>
                {e}
              </option>
            ))}
          </HTMLSelect>
        </Navbar.Group>
      </Navbar>
    );
  }
}

export default NavigationBar;
