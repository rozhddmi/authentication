import { MTable, IUser, MBataBaseElement } from "./interfaces";
import * as React from "react";
import { FormDialogRendere } from "./components";
import { LocalizedStrings } from "localized-strings";
import { FormButtonDialog } from "./components/form/form_button_dialog";
import { TableView } from "./components/table/table_view";
import {
  IFormBody,
  IFormButtonDialog,
  IFormDialog
} from "./components/form/interfaces";
import { FormRenderer } from "./components/form/form_body";
import { ITableView } from "./components/table/interfaces";
import { action, observable, computed } from "mobx";
import { Button, Toaster, Position, Intent } from "@blueprintjs/core";
import { checkRights } from "../../../common/functions";
import Axios from "axios";
import { IFilter } from "./components/filters/interface";
import { castFilters } from "./components/filters/functions";
import {
  SavableClass,
  SavableObservable
} from "../../savable_store/decorator_factory";

interface ApiRoutes {
  get: string;
  post?: string;
  update?: string;
  delete?: string;
  import?: string;
}

@SavableClass
export class ShemaRenderer {
  static Toaster = Toaster.create({
    className: "recipe-toaster",
    position: Position.TOP
  });

  @observable dialog: JSX.Element | null = null;
  table_name: string;
  table: MTable;
  localizator: LocalizedStrings<any>;
  @observable user: IUser | undefined;
  @observable table_updater: number = Math.random();
  private routs: ApiRoutes;
  @SavableObservable filters: IFilter[] = [];
  @SavableObservable filter_is_open: boolean = false;

  constructor(
    table: MTable,
    user: IUser | undefined,
    localizator: LocalizedStrings<any>,
    routs: ApiRoutes
  ) {
    this.table_name = table.name;
    this.table = table;
    this.localizator = localizator;
    this.user = user;
    this.routs = routs;
  }

  @computed
  get filter_group(): number[] {
    return [...new Set(this.filters.map(f => f.group))];
  }

  @action.bound
  setFilterIsOpen(state: boolean) {
    this.filter_is_open = state;
  }

  @action.bound
  filterReorder(start, end, old_group, new_group) {
    const removed = this.filters
      .map((e, i) => (e.group === old_group ? i : -1))
      .filter(i => i >= 0);
    const add_to = this.filters
      .map((e, i) => (e.group === new_group ? i : -1))
      .filter(i => i >= 0);
    const startIndex = removed[start];
    const endIndex = add_to.length > end ? add_to[end] : this.filters.length;

    const [element] = this.filters.splice(startIndex, 1);
    element.group = new_group;
    this.filters.splice(endIndex!, 0, element);
  }

  @action.bound
  setFilter(
    index?: number,
    filter: IFilter = {
      operator: "~",
      field: "",
      value: "",
      logic: "AND",
      group: -1
    }
  ) {
    filter.field = filter.field || this.keys[0] || "";

    index = index === undefined ? this.filters.length : index;

    if (filter.group < 0) {
      filter.group =
        this.filters[index - 1] && this.filters[index - 1].logic === "AND"
          ? Math.max(0, ...this.filter_group)
          : Math.max(-1, ...this.filter_group) + 1;
    }
    this.filters[index] = filter;
    console.log(JSON.stringify(this.filters));
  }

  castFilters = () => {
    return castFilters(this.filters);
  };

  @action.bound
  removeFilter(index: number) {
    this.filters.splice(index, 1);
  }

  @computed
  get keys() {
    return Object.keys(this.table.schema);
  }
  getAvaliableKeys = (method: "post" | "get" | "update" | "delete") => {
    return this.keys.filter(field =>
      checkRights(this.table, this.user, method, field)
    );
  };

  @action.bound
  setNewUser(user: IUser | undefined) {
    this.user = user;
  }

  @computed
  get can_edit() {
    return checkRights(this.table, this.user, "update") && this.routs.update;
  }

  @computed
  get can_delete() {
    return checkRights(this.table, this.user, "delete") && this.routs.delete;
  }

  @computed
  get can_add() {
    return checkRights(this.table, this.user, "post") && this.routs.post;
  }

  @computed
  get can_export() {
    return checkRights(this.table, this.user, "export");
  }

  @computed
  get can_import() {
    return checkRights(this.table, this.user, "import");
  }

  localize = (text: string | undefined) => {
    if (text === undefined) return undefined;
    return this.localizator
      ? this.localizator.getString(text, undefined, true) || text
      : text;
  };

  createForm(renderer: IFormBody): JSX.Element {
    //console.log("[SCHEMA RENDERER] : createForm", renderer);
    return <FormRenderer {...renderer} schema_store={this} />;
  }

  createFormButtonDialog(renderer: IFormButtonDialog): JSX.Element {
    //console.log("[SCHEMA RENDERER] : createFormDialog", renderer);
    return <FormButtonDialog {...renderer} schema_store={this} />;
  }

  createDialog(renderer: IFormDialog): JSX.Element {
    return <FormDialogRendere {...renderer} schema_store={this} />;
  }

  createAddButton = (): JSX.Element => {
    return this.createFormButtonDialog({
      method: "post",
      form_lable:
        this.localize("ls-AddForm") + " " + this.localize(this.table_name),
      dialog_button_icon: "add",
      dialog_button_text: "ls-Add",
      button_text: "ls-Save",
      disabled_keys: Object.keys(this.table.schema).filter(key => {
        return !checkRights(this.table, this.user, "post", key);
      }),
      post_url: {
        address: this.routs.post!,
        object_as_config: true,
        success_function: (res, obj) => {
          ShemaRenderer.Toaster.show({ message: "ADDED", intent: Intent.NONE });
          this.table_updater = Math.random();
        },
        fail_contain_object_error: true
      }
    });
  };

  createTableView(renderer: ITableView): JSX.Element {
    return <TableView {...renderer} schema_store={this} />;
  }

  @action
  private _onTableButtonClick = e => {
    const [action, id] = e.currentTarget.id.split(":");
    if (action === "edit") {
      this.dialog = this.createDialog({
        method: "update",
        usePortal: true,
        isOpen: true,
        disabled_keys: Object.keys(this.table.schema).filter(key => {
          return !checkRights(this.table, this.user, "update", key);
        }),
        form_lable: this.localize("ls-editElement"),
        button_text: this.localize("ls-save"),
        onClose: e => (this.dialog = null),
        fetch_url: {
          address: this.routs.get,
          config: { id: id }
        },
        post_url: this.routs.update
          ? {
              address: this.routs.update,
              object_as_config: true,
              fail_contain_object_error: true,
              success_function: (ax_o, o) => {
                this.dialog = null;
                this.table_updater = Math.random();
              }
            }
          : undefined
      });
    } else if (action === "delete") {
      ShemaRenderer.Toaster.show({
        action: {
          onClick: () => {
            Axios.delete(this.routs.delete!, { params: { id: id } })
              .then(e => {
                ShemaRenderer.Toaster.show({
                  message: this.localize("ls-deleted")
                });
                this.table_updater = Math.random();
              })
              .catch(e =>
                ShemaRenderer.Toaster.show({
                  message: e
                })
              );
          },
          text: this.localize("ls-Delete")
        },
        icon: "delete",
        intent: Intent.WARNING,
        message: this.localize("ls-confirmDelete")
      });
    }
  };

  getEditButton = (element: MBataBaseElement, onClick?: { (e): void }) => {
    return (
      <Button
        small
        minimal
        icon="edit"
        id={"edit:" + element._id}
        onClick={onClick || this._onTableButtonClick}
      />
    );
  };

  getDeleteButton = (element: MBataBaseElement, onClick?: { (e): void }) => {
    return (
      <Button
        small
        minimal
        icon="delete"
        id={"delete:" + element._id}
        onClick={onClick || this._onTableButtonClick}
      />
    );
  };

  getRefLink = (
    element: MBataBaseElement,
    shema_key: string,
    onClick?: { (e): void }
  ) => {
    const { key } = this.table.schema[shema_key].renderer!;
    return (
      <a
        id={`ref-clicked:${element._id}:${shema_key}`}
        onClick={onClick || this._onTableButtonClick}
      >
        {key ? key(element) : ""}
      </a>
    );
  };
}
