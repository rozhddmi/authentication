import { ShemaRenderer } from "../..";

import { observer } from "mobx-react";

import React from "react";

import {
  Droppable,
  DroppableProvided,
  DroppableStateSnapshot
} from "react-beautiful-dnd";

import { Classes } from "@blueprintjs/core";

import { FilterElement, LogicSelector } from "./filter_element";

interface IFilterGroupProps {
  schema_store: ShemaRenderer;
  keys: string[];
  group: number;
}

export const getDropableStyle = (
  provided: DroppableProvided,
  snapshot: DroppableStateSnapshot
) => {
  console.log(provided, snapshot);
  return {
    background: snapshot.isDraggingOver ? "lightgrey" : undefined
  };
};

@observer
export class FilterGroupt extends React.Component<IFilterGroupProps> {
  static MARGIN = 10;
  static PADDING = 5;

  getHeight(
    provided: DroppableProvided,
    snapshot: DroppableStateSnapshot,
    filter_index
  ) {
    const el_height = FilterElement.HEIGHT + FilterElement.MARGIN;
    return (
      filter_index.length * el_height +
      FilterElement.MARGIN +
      FilterGroupt.PADDING * 2 +
      (snapshot.isDraggingOver && !snapshot.draggingFromThisWith
        ? el_height
        : 0)
    );
  }
  render() {
    const { group, schema_store, keys } = this.props;
    const { filters } = schema_store!;
    const filter_index = filters
      .map((e, i) => {
        return e.group === group ? i : undefined;
      })
      .filter(e => e !== undefined);
    return (
      <>
        <Droppable droppableId={group.toString()}>
          {(provided, snapshot) => (
            <div
              className={Classes.CARD}
              style={{
                padding: FilterGroupt.PADDING,
                margin: FilterGroupt.MARGIN,
                minHeight: this.getHeight(provided, snapshot, filter_index),
                ...getDropableStyle(provided, snapshot)
              }}
              ref={provided.innerRef}
            >
              {filter_index.map((fi, ci) => (
                <FilterElement
                  key={fi}
                  schema_store={schema_store}
                  keys={keys}
                  index={fi!}
                  conseq_index={ci}
                  last_index={filter_index.last()!}
                />
              ))}
            </div>
          )}
        </Droppable>
        <LogicSelector
          {...this.props}
          index={filter_index.last()!}
          conseq_index={filter_index.last()!}
        />
      </>
    );
  }
}
