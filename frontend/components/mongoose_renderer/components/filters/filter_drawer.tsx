import {
  Drawer,
  IButtonProps,
  ITooltipProps,
  Position,
  Classes,
  ControlGroup
} from "@blueprintjs/core";
import React from "react";
import { ShemaRenderer } from "../..";

import { Button, Tooltip } from "@blueprintjs/core";
import { observer } from "mobx-react";
import { action } from "mobx";
import { FilterElement } from "./filter_element";
import {
  DragDropContext,
  Droppable,
  DropResult,
  ResponderProvided
} from "react-beautiful-dnd";
import { FilterGroupt, getDropableStyle } from "./filter_group";

interface IFormDrawerButtonInt extends Partial<IButtonProps>, ITooltipProps {
  schema_store: ShemaRenderer;
  keys: string[];
}

@observer
export class DrawerButton extends React.Component<IFormDrawerButtonInt, {}> {
  constructor(props: IFormDrawerButtonInt) {
    super(props);
  }

  @action
  private handleOpen = () => {
    this.props.schema_store.setFilterIsOpen(true);
  };

  @action
  private handleClose = () => {
    this.props.schema_store.setFilterIsOpen(false);
  };
  onDragEnd = (result: DropResult, provider: ResponderProvided) => {
    const { source, destination } = result;
    const { filterReorder } = this.props.schema_store;
    if (!destination) {
      console.log("NO DESTINATION");
      return;
    }
    filterReorder(
      source.index,
      destination.index,
      Number(source.droppableId),
      Number(destination.droppableId)
    );
  };

  render() {
    const { schema_store } = this.props;
    const {
      localize,
      filter_group,
      castFilters,
      filter_is_open
    } = schema_store;
    return (
      <>
        <Tooltip {...this.props}>
          <Button {...this.props} onClick={this.handleOpen} />
        </Tooltip>
        <Drawer
          isOpen={filter_is_open}
          size={600}
          position={Position.RIGHT}
          //className={this.props.schema_store!.the}
          icon="info-sign"
          onClose={this.handleClose}
          title={localize("ls-filterDrawer")}
        >
          <div className={Classes.DRAWER_BODY}>
            <div className={Classes.DIALOG_BODY}>
              <DragDropContext onDragEnd={this.onDragEnd}>
                {filter_group.map((e, i) => (
                  <FilterGroupt
                    key={i}
                    schema_store={this.props.schema_store}
                    group={e}
                    keys={this.props.keys}
                  />
                ))}
                <Droppable droppableId={(filter_group.last() + 1).toString()}>
                  {(provided, snapshot) => (
                    <div
                      className={Classes.CARD}
                      style={{
                        padding: 5,
                        margin: 10,
                        minHeight:
                          FilterElement.HEIGHT + 2 * FilterElement.MARGIN,
                        ...getDropableStyle(provided, snapshot)
                      }}
                      ref={provided.innerRef}
                    ></div>
                  )}
                </Droppable>
              </DragDropContext>
            </div>
          </div>
          <div className={Classes.DRAWER_FOOTER}>
            <ControlGroup>
              <Button
                fill
                icon={"add"}
                text={localize("ls-addFilter")}
                onClick={() => schema_store!.setFilter()}
              />

              <Button
                text="Apply"
                fill
                icon="filter"
                onClick={() => {
                  console.log(castFilters());
                }}
              />
            </ControlGroup>
          </div>
        </Drawer>
      </>
    );
  }
}
