import {
  HTMLSelect,
  InputGroup,
  Button,
  Classes,
  ControlGroup
} from "@blueprintjs/core";
import React from "react";
import { ShemaRenderer } from "../..";
import { observer } from "mobx-react";
import { LOGIC_OPERATOR_SET, LOGIC_SET } from "./interface";
import { Draggable } from "react-beautiful-dnd";

interface ISearchProps {
  index: number;
  conseq_index: number;
  last_index?: number;
  schema_store: ShemaRenderer;
  keys: string[];
}

export const LogicSelector = (props: ISearchProps) => {
  const { index, schema_store } = props;
  const { filters } = schema_store!;
  const value = filters[index];

  const onChange = e => {
    const { id, value } = e.currentTarget as any;
    const filter_value = filters[index];
    schema_store.setFilter(index, {
      ...filter_value,
      [id]: value
    });
  };

  return (
    <div style={{ alignContent: "center", padding: 10 }}>
      <HTMLSelect value={value.logic} onChange={onChange} id="logic" fill>
        {LOGIC_SET.map(e => (
          <option key={e} value={e}>
            {e}
          </option>
        ))}
      </HTMLSelect>
    </div>
  );
};

@observer
export class FilterElement extends React.Component<ISearchProps> {
  static HEIGHT = 98;
  static MARGIN = 10;

  updateFilter = (e: React.FormEvent<HTMLElement>) => {
    const { id, value } = e.currentTarget as any;

    const { index, schema_store } = this.props;
    const { filters } = schema_store!;
    const filter_value = filters[index];

    console.log(id, value);
    this.props.schema_store!.setFilter(this.props.index, {
      ...filter_value,
      [id]: value
    });
  };

  render() {
    const { index, schema_store, last_index, conseq_index } = this.props;
    const { filters, table } = schema_store!;
    const { schema } = table;
    const value = filters[index];
    const tp = value.field ? schema[value.field].type : String;

    return (
      <Draggable draggableId={index.toString()} index={conseq_index}>
        {(provided, snapshot) => (
          <div
            className={Classes.CARD}
            ref={provided.innerRef}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
            style={{
              height: FilterElement.HEIGHT,
              userSelect: "none",
              marginBottom: FilterElement.MARGIN,
              marginTop: FilterElement.MARGIN,
              padding: 10,
              background: !snapshot.isDragging ? undefined : "grey",
              ...(provided.draggableProps.style || {})
            }}
          >
            <ControlGroup fill>
              <HTMLSelect
                value={value.field}
                onChange={this.updateFilter}
                id="field"
              >
                {this.props.keys.map(key => (
                  <option key={key} value={key}>
                    {" "}
                    {schema[key].renderer!.label || key}
                  </option>
                ))}
              </HTMLSelect>

              <HTMLSelect
                value={value.operator}
                onChange={this.updateFilter}
                id="operator"
              >
                {LOGIC_OPERATOR_SET.map(e => (
                  <option key={e} value={e}>
                    {e}
                  </option>
                ))}
              </HTMLSelect>

              <InputGroup
                {...this.props}
                leftIcon="search"
                id="value"
                width={100}
                type={
                  tp === (Date as any)
                    ? "datetime-local"
                    : tp === (Number as any)
                    ? "number"
                    : "text"
                }
                placeholder="Filter table"
                value={value.value}
                onChange={this.updateFilter}
              />
              <Button
                minimal
                icon="delete"
                onClick={() => schema_store!.removeFilter(index)}
              />
            </ControlGroup>
            {last_index !== index ? <LogicSelector {...this.props} /> : null}
          </div>
        )}
      </Draggable>
    );
  }
}
