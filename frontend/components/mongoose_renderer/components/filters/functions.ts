import { ILogic, ILogicOperator, IFilter } from "./interface";

export function castLogic(logic: ILogic) {
  switch (logic) {
    case "AND":
      return "$and";
    case "OR":
      return "$or";
    default:
      return "";
  }
}

export function castOperator(operator: ILogicOperator) {
  switch (operator) {
    case ">=":
      return "$gte";
    case ">":
      return "$gt";
    case "<":
      return "$lt";
    case "<=":
      return "$lte";
    case "==":
      return "$eq";
    case "!=":
      return "$ne";
    case "~":
      return "$regex";
    default:
      return "";
  }
}

function castFilter(filter: IFilter) {
  return { [filter.field]: { [castOperator(filter.operator)]: filter.value } };
}

export function castFilters(filters: IFilter[]) {
  let obj: { [key: string]: any } = {};
  let prev_logic: string | undefined = undefined;
  for (let index = 0; index < filters.length; index++) {
    const filter = filters[index];
    const next_exist = index + 1 < filters.length;
    const logic = castLogic(filter.logic);
    if (!next_exist && index === 0) {
      obj = castFilter(filter);
    } else if (index === 0) {
      obj = { [logic]: [castFilter(filter)] };
      prev_logic = logic;
    } else if (next_exist && logic) {
      if (logic !== prev_logic) {
        const new_obj = { [logic]: [castFilter(filter)] };
        if (prev_logic && obj[prev_logic]) {
          obj[prev_logic].push(new_obj);
          obj = new_obj;
        }
        prev_logic = logic;
      } else {
        obj[logic].push(castFilter(filter));
      }
    } else if (prev_logic) {
      obj[prev_logic].push(castFilter(filter));
    }
  }
  return obj;
}
