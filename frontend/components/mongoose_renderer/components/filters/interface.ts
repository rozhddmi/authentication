export type ILogic = "AND" | "OR" | "";
export const LOGIC_SET: ILogic[] = ["AND", "OR", ""];
export type ILogicOperator = ">=" | ">" | "<" | "<=" | "==" | "~" | "!=";
export const LOGIC_OPERATOR_SET: ILogicOperator[] = [
  ">=",
  ">",
  "<",
  "<=",
  "==",
  "~",
  "!="
];
export interface IFilter {
  value: any;
  operator: ILogicOperator;
  logic: ILogic;
  group: number;
  field: string;
}
