import { PostFormAxioMongoose, IBaseMongoose } from "../../interfaces";

import { IconName, IDialogProps } from "@blueprintjs/core";

export type IFormState = {
  loading: boolean;
  errors: { [key: string]: string };
  obj: { [key: string]: string };
};

export type IFormAction =
  | { type: "global"; value: IFormState }
  | { type: "error_update"; errors: { [key: string]: string } }
  | { type: "object_update"; value: { [key: string]: string } }
  | { type: "loading_update"; value: boolean };

export interface IFormBody extends IBaseMongoose {
  obj?: { [key: string]: string };
  form_lable?: string;
  form_footer?: string;
  button_text?: string;
  post_url?: PostFormAxioMongoose;
  method: "post" | "update";
  closeDialog: Function;
}

export interface IFormButtonDialog extends Partial<IFormBody> {
  dialog_button_icon?: IconName;
  dialog_button_text?: string;
  dialog_button_tooltip?: string;
  uncloasable?: boolean;
  theme?: string;
  method: "post" | "update";
}

export interface IFormElementRenderer {
  obj_key: string;
  setter: { (key: string, value: any): void };
  error?: string;
  disabled?: boolean;
  obj: Object;
}

export interface IFormDialog extends IDialogProps, IFormButtonDialog {}
