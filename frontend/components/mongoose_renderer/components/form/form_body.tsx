import { PostFormAxioMongoose, ShemaRenderer } from "../../index";

import React from "react";

import { IFormBody } from "./interfaces";

import { Classes, Button } from "@blueprintjs/core";

import { FormElement } from "../index";
import { createDefault, filterKeys } from "./functions";
import axios from "axios";
import { observable, action, computed } from "mobx";
import { observer, inject } from "mobx-react";
import { checkRights } from "../../../../../common/functions";

interface IFormBodyInt extends IFormBody {
  schema_store: ShemaRenderer;
}

@observer
export class FormRenderer extends React.Component<IFormBodyInt, {}> {
  @observable private errors: { [key: string]: string } = {};
  @observable private _object: { [key: string]: string } = {};
  @observable private loading: boolean = false;

  constructor(props: IFormBodyInt) {
    super(props);
    if (props.fetch_url) {
      axios
        .get(props.fetch_url.address, { params: { ...props.fetch_url.config } })
        .then(
          action((result: any) => {
            this._object = result.data;
          })
        );
    } else {
      this._object =
        this.props.obj || createDefault(this.props.schema_store!.table.schema);
    }
  }

  @computed
  get object(): { [key: string]: string } {
    const { schema_store, keys, exclude_keys } = this.props;
    return filterKeys(
      this._object,
      schema_store!.table.schema,
      keys,
      exclude_keys
    );
  }

  @computed
  get keys(): string[] {
    return Object.keys(this.object);
  }

  @action
  onSubmit = ev => {
    const {
      address: adress,
      config,
      object_as_config,
      fail_function,
      success_function,
      fail_contain_object_error
    } = this.props.post_url || ({} as PostFormAxioMongoose);
    ev.preventDefault();
    this.loading = true;
    this.errors = {};
    // removed protected
    const { schema_store, method } = this.props;
    const { table, user } = schema_store!;
    const send_obj = { _id: this.object._id };
    for (const key of Object.keys(this.object)) {
      if (
        this.object[key] !== undefined &&
        checkRights(table, user, method, key)
      ) {
        send_obj[key] = this.object[key];
      }
    }
    axios
      .post(adress, config || object_as_config ? send_obj : {})
      .then(
        action((res: any) => {
          // Save to localStorage
          success_function && success_function(res, this.object);
          this.loading = false;
          this.errors = {};
          this.props.closeDialog();
        })
      )
      .catch(
        action((err: any) => {
          fail_function && fail_function(err, this.object);
          if (fail_contain_object_error) {
            this.loading = false;
            this.errors = (err.response && err.response.data) || {};
          }
        })
      );
  };

  @action.bound
  setValue(key: string, value: any) {
    this._object[key] = value;
  }

  render() {
    console.log("FOrm body render");
    const { localize = v => v } = this.props.schema_store!;
    const { button_text, form_footer, disabled_keys = [] } = this.props;

    return (
      <>
        <div className={Classes.DIALOG_BODY}>
          <form noValidate onSubmit={this.onSubmit}>
            {this.keys.map(v =>
              v.startsWith("_") ? null : (
                <FormElement
                  schema_store={this.props.schema_store}
                  obj={this.object}
                  setter={this.setValue}
                  key={v}
                  obj_key={v}
                  error={this.errors[v]}
                  disabled={disabled_keys.includes(v)}
                />
              )
            )}

            <Button
              loading={this.loading}
              type="submit"
              text={localize(button_text) || "Submit"}
            />
          </form>
          <div className={Classes.DIALOG_FOOTER}>
            <div className={Classes.DIALOG_FOOTER_ACTIONS}>
              {localize(form_footer)}
            </div>
          </div>
        </div>
      </>
    );
  }
}
