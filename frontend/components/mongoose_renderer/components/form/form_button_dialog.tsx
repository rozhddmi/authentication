import * as React from "react";

import { Button, Tooltip } from "@blueprintjs/core";
import { FormDialogRendere } from "./form_dialog";
import { IFormButtonDialog } from "./interfaces";
import { inject, observer } from "mobx-react";
import { observable, action } from "mobx";
import { ShemaRenderer } from "../../schema_renderer";

interface IFormDialogButtonInt extends IFormButtonDialog {
  schema_store: ShemaRenderer;
}

@observer
export class FormButtonDialog extends React.Component<
  IFormDialogButtonInt,
  {}
> {
  @observable private isOpen: boolean = false;

  @action
  private handleOpen = () => {
    this.isOpen = true;
  };

  @action
  private handleClose = () => {
    this.isOpen = false;
  };

  render() {
    const {
      dialog_button_tooltip,
      dialog_button_icon,
      dialog_button_text
    } = this.props;
    return (
      <>
        <Tooltip
          content={dialog_button_tooltip}
          disabled={!dialog_button_tooltip}
        >
          <Button
            minimal
            text={dialog_button_text}
            onClick={this.handleOpen}
            icon={dialog_button_icon}
          />
        </Tooltip>
        <FormDialogRendere
          {...this.props}
          closeDialog={this.handleClose}
          isOpen={this.isOpen}
          onClose={this.handleClose}
        />
      </>
    );
  }
}
