import * as React from "react";
import { Dialog } from "@blueprintjs/core";
import { FormRenderer } from "./form_body";
import { IFormDialog } from "./interfaces";
import { inject, observer } from "mobx-react";
import { ShemaRenderer } from "../../schema_renderer";
import { observable, action } from "mobx";

interface IFormDialogInt extends IFormDialog {
  schema_store: ShemaRenderer;
}

@observer
export class FormDialogRendere extends React.Component<IFormDialogInt> {
  @observable isOpen: boolean = true;

  @action
  closeDialog() {
    this.isOpen = false;
  }
  constructor(props) {
    super(props);
  }
  render() {
    const { theme, uncloasable, form_lable } = this.props;
    return (
      <Dialog
        {...this.props}
        isOpen={this.props.isOpen && this.isOpen}
        className={theme}
        canEscapeKeyClose={!uncloasable}
        canOutsideClickClose={!uncloasable}
        isCloseButtonShown={!uncloasable}
        title={this.props.schema_store!.localize(form_lable || "Form")}
      >
        {this.props.isOpen ? (
          <FormRenderer {...this.props} closeDialog={this.closeDialog} />
        ) : null}
      </Dialog>
    );
  }
}
