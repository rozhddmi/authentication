import * as React from "react";
import {
  FormGroup,
  InputGroup,
  Intent,
  Tooltip,
  Button,
  HTMLSelect
} from "@blueprintjs/core";
import { IFormElementRenderer } from "./interfaces";
import { isObjectId, getType } from "./functions";
import { observable, action } from "mobx";
import { observer, inject } from "mobx-react";
import { ShemaRenderer } from "../../schema_renderer";

const LockButton: React.FunctionComponent<{
  show_password: boolean;
  onClick: { (state: boolean): void };
  localize: { (v: string): string };
}> = props => {
  return (
    <Tooltip
      content={props.localize(
        props.show_password ? "hidePassword" : "showPassword"
      )}
    >
      <Button
        icon={props.show_password ? "unlock" : "lock"}
        intent={Intent.WARNING}
        minimal={true}
        onClick={() => props.onClick(!props.show_password)}
      />
    </Tooltip>
  );
};

interface IForElementInt extends IFormElementRenderer {
  schema_store: ShemaRenderer;
}

@observer
export class FormElement extends React.Component<IForElementInt, {}> {
  @observable private show_password: boolean = false;

  @action.bound
  setShowPassword(state: boolean) {
    this.show_password = state;
  }

  onChange = e => {
    this.props.setter(this.props.obj_key, e.target.value);
  };

  render() {
    const { obj_key, error, disabled, schema_store } = this.props;
    const { obj: object } = this.props;
    const { table, localize = v => v } = schema_store!;
    const { schema } = table!;
    if (schema === undefined) {
      return null;
    }
    const schema_element = schema[obj_key];
    const {
      help = undefined,
      label = obj_key,
      email_field = false,
      password_field = false
    } = schema_element.renderer || {};

    const { type, required } = schema_element;
    let input_type = getType(type);
    input_type = email_field
      ? "email"
      : password_field && !this.show_password
      ? "password"
      : input_type;

    return (
      <FormGroup
        intent={error ? Intent.WARNING : Intent.PRIMARY}
        label={localize(label || obj_key)}
        labelInfo={localize(required ? "required" : undefined)}
        helperText={error || localize(help)}
      >
        {schema_element.enum ? (
          <HTMLSelect
            fill
            value={object[obj_key] || schema_element.enum[0]}
            onChange={this.onChange}
          >
            {schema_element.enum.map(e => (
              <option key={e} value={e}>
                {e}
              </option>
            ))}
          </HTMLSelect>
        ) : isObjectId(schema_element.type) ? null : (
          <InputGroup
            name={obj_key}
            intent={error ? Intent.WARNING : Intent.PRIMARY}
            leftIcon={error ? "error" : undefined}
            rightElement={
              password_field ? (
                <LockButton
                  show_password={this.show_password}
                  onClick={this.setShowPassword}
                  localize={localize}
                />
              ) : (
                undefined
              )
            }
            onChange={this.onChange}
            value={object[obj_key]}
            id={obj_key}
            type={input_type}
            disabled={disabled}
          />
        )}
      </FormGroup>
    );
  }
}
