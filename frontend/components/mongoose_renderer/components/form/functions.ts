import { MSchema } from "../../index";
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

export const dateToInput = (date?: Date) => {
  date = date || new Date();
  const iso = date.toISOString();
  return iso.substring(0, iso.length - 1);
};

export const filterKeys = (
  obj: Object,
  schema: MSchema,
  keys?: string[],
  excluded_keys: string[] = []
) => {
  excluded_keys = excluded_keys || [];
  const ret_obj = {};
  keys = keys || Object.keys(schema);
  for (let key of keys) {
    if (keys.includes(key) && !excluded_keys.includes(key))
      ret_obj[key] = obj[key];
  }
  ret_obj["_id"] = obj["_id"];
  return ret_obj;
};

export const createDefault = (
  schema: MSchema | undefined,
  keys?: string[],
  exclude_keys?: string[]
): { [key: string]: string } => {
  const ret_obj = {};
  if (schema === undefined) {
    return ret_obj;
  }
  exclude_keys = exclude_keys || [];
  keys = keys || Object.keys(schema);
  for (let key of keys) {
    if (key.startsWith("_") || exclude_keys.includes(key)) {
      continue;
    }
    let element = schema[key];
    switch (schema[key].type as any) {
      case Date:
        ret_obj[key] = dateToInput();
        break;
      case Number:
        ret_obj[key] = element.default || 0;
        break;
      case Boolean:
        ret_obj[key] = element.default || false;
        break;
      case String:
      default:
        ret_obj[key] = element.default || "";
    }
  }
  return ret_obj;
};

export const getType = (type: any) => {
  switch (type) {
    case Date:
      return "datetime-local";
    case Number:
      return "number";
    case Boolean:
      return "checkbox";
    case String:
    default:
      return "text";
  }
};

export const isObjectId = (type: any) => {
  return type === Schema.Types.ObjectId;
};
