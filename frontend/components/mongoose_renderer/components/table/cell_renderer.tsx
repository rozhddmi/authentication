import { CellMeasurerCache, CellMeasurer } from "react-virtualized";
import React from "react";
import { Checkbox } from "@blueprintjs/core";
import { inject } from "mobx-react";
import { ShemaRenderer } from "../../schema_renderer";
import { MBataBaseElement } from "../../interfaces";

const { Schema } = require("mongoose");

const cellStyle = {
  justifyContent: "center",
  padding: ".5em",
  borderLeft: "1px solid #e0e0e0",
  borderBottom: "1px solid #e0e0e0"
};

export const cache = new CellMeasurerCache({
  defaultWidth: 100,
  minWidth: 20,
  fixedHeight: true
});

export const CellRenderer = inject("schema_store")(
  (props: {
    columnIndex: number;
    isScrolling: boolean;
    isVisible: boolean;
    key: number | string;
    parent;
    rowIndex: number;
    style;
    elements: MBataBaseElement[];
    keys: string[];
    fix_column_count: number;
    schema_store: ShemaRenderer;
    onClick: { (e): void };
  }) => {
    const {
      columnIndex,
      key,
      parent,
      rowIndex,
      style,
      elements,
      keys,
      fix_column_count,
      schema_store,
      onClick
    } = props;
    const { schema } = schema_store!.table;
    const {
      localize,
      can_edit,
      can_delete,
      getDeleteButton,
      getEditButton,
      getRefLink
    } = schema_store!;

    let content: JSX.Element | null = null;
    let extra_style = {};
    const shema_key = keys[columnIndex - fix_column_count];
    const { renderer = {} } = schema[shema_key] || {};
    const { label = shema_key } = renderer;
    if (rowIndex === 0) {
      extra_style = {
        ...extra_style,
        borderBottom: "none"
      };
      if (columnIndex >= fix_column_count) {
        content = localize(label);
      } else {
        content = null;
        extra_style = {
          ...extra_style,
          borderBottom: "none",
          borderLeft: "none"
        };
      }
    } else {
      const row_index = rowIndex - 1;
      const element = elements[row_index];
      if (columnIndex === 0) {
        content = <Checkbox key={key} id={rowIndex + ":" + columnIndex} />;
      } else if (columnIndex === 1 && can_delete) {
        content = getDeleteButton(element);
      } else if (
        can_edit &&
        (columnIndex === 2 || (!can_delete && columnIndex === 1))
      ) {
        content = getEditButton(element);
      } else if (schema[shema_key].type === Schema.Types.ObjectId) {
        content = getRefLink(element, shema_key, onClick);
      } else {
        content = elements[row_index][shema_key];
      }
    }

    return (
      <CellMeasurer
        cache={cache}
        columnIndex={columnIndex}
        key={key}
        parent={parent}
        rowIndex={rowIndex}
      >
        <div
          style={{
            fontWeight: rowIndex === 0 ? "bold" : "normal",
            textAlign: "center",
            ...style,
            ...(rowIndex < 2 || rowIndex % 2 ? cellStyle : cellStyleEven),
            ...extra_style,
            whiteSpace: "nowrap"
          }}
        >
          {content}
        </div>
      </CellMeasurer>
    );
  }
);

const cellStyleEven = {
  ...cellStyle,
  background: "lightgrey"
};
