import { IBaseMongoose, PostFormAxioMongoose } from "../../index";
import { MBataBaseElement } from "../../interfaces";

export interface ITableView extends IBaseMongoose {
  elements?: MBataBaseElement[];
  post_url?: PostFormAxioMongoose;
}
