import * as React from "react";
import {
  Intent,
  Toaster,
  Toast,
  Navbar,
  Alignment,
  Tooltip,
  Button,
  IButtonProps
} from "@blueprintjs/core";
import axios from "axios";
import { AutoSizer, MultiGrid } from "react-virtualized";
import "react-virtualized/styles.css"; // only needs to be imported once
import { cache, CellRenderer } from "./cell_renderer";
import { ITableView } from "./interfaces";
import { MBataBaseElement } from "../../interfaces";
import { observer } from "mobx-react";
import { observable, action, autorun } from "mobx";
import { ShemaRenderer } from "../../schema_renderer";
import { parseAsync } from "json2csv";
import { isObjectId } from "../form/functions";
import { saveAs } from "file-saver";
import { DrawerButton } from "../filters/filter_drawer";
import { SavableClass } from "../../../../savable_store/decorator_factory";
import { ImportDialogButton } from "../../../dashboard_components/import_table";

const CONTROL_HEIGHT = 40;
interface ITableViewInt extends ITableView {
  schema_store: ShemaRenderer;
}

interface IAsyncButton extends IButtonProps {
  onClick: { (): Promise<any> };
  onSucces?: Function;
  onFail?: Function;
}

@observer
export class PromisButton extends React.Component<IAsyncButton, {}> {
  @observable is_loading: boolean;
  @observable errors: string | undefined;

  onClick = e => {
    if (this.is_loading) return;
    this.is_loading = true;
    this.props
      .onClick()
      .then(() => {
        this.is_loading = false;
        this.errors = undefined;
      })
      .catch(e => {
        this.is_loading = false;
        this.errors = e;
      });
  };

  render() {
    return (
      <Tooltip content={this.errors || ""} disabled={this.errors === undefined}>
        <Button
          {...this.props}
          onClick={this.onClick}
          loading={this.is_loading}
          intent={this.errors ? Intent.DANGER : Intent.NONE}
        />
      </Tooltip>
    );
  }
}

@observer
@SavableClass
export class TableView extends React.Component<ITableViewInt, {}> {
  @observable private error: string;
  @observable private elements: MBataBaseElement[] = [];
  @observable private loading: boolean = false;

  constructor(props: ITableViewInt) {
    super(props);
    autorun(() => {
      const rand = props.schema_store!.table_updater;
      if (props.fetch_url && rand >= 0) {
        this.loading = true;
        this.error = "";
        axios
          .get(props.fetch_url.address, props.fetch_url.config)
          .then(
            action((result: any) => {
              this.error = "";
              this.loading = false;
              this.elements = result.data;
            })
          )
          .catch(
            action((err: any) => {
              this.error = err.response.data;
              this.loading = false;
              this.elements.length = 0;
            })
          );
      } else {
        this.elements = this.props.elements || [];
      }
    });
  }

  get keys(): string[] {
    const { keys, exclude_keys, schema_store } = this.props;
    const { getAvaliableKeys } = schema_store!;
    return keys || exclude_keys
      ? getAvaliableKeys("get").filter(e => !exclude_keys!.includes(e))
      : getAvaliableKeys("get");
  }

  @action
  onClick = e => {
    const { on_ref_click, schema_store } = this.props;
    const { table } = schema_store!;
    const [action, index, field] = e.currentTarget.id.split(":");
    if (action === "ref-clicked") {
      const obj = this.elements[Number(index)][field];
      const elem_object = table!.schema[field];
      if (on_ref_click) {
        on_ref_click(obj, elem_object, field);
      }
      //setter({ ...fiterState, edited_object: obj });
    }
  };

  export = async () => {
    const { schema } = this.props.schema_store!.table;
    return new Promise<string>((resolve, reject) => {
      const objFun: Map<string, Function> = new Map();
      for (let index = 0; index < this.keys.length; index++) {
        const key = this.keys[index];
        if (
          isObjectId(schema[key].type) &&
          schema[key].renderer &&
          schema[key].renderer!.key
        ) {
          objFun.set(key, schema[key].renderer!.key!);
        }
      }

      const newElems =
        objFun.size === 0
          ? this.elements
          : this.elements.map(element => {
              const new_el = { ...element };
              objFun.forEach((fun, key) => {
                new_el[key] = fun(element);
              });
              return new_el;
            });
      parseAsync(newElems, { fields: this.keys })
        .then(csv => {
          console.log(csv);
          const blob = new Blob([csv], { type: "text/plain;charset=utf-8" });
          saveAs(blob, this.props.schema_store!.table_name + "_export.csv");
          resolve(csv);
        })
        .catch(e => {
          reject(e);
        });
    });
  };

  render() {
    const { schema_store } = this.props;
    const {
      localize,
      can_edit,
      can_delete,
      can_add,
      can_export,
      can_import,
      createAddButton
    } = schema_store!;
    const fix_column_count = (can_edit ? 1 : 0) + (can_delete ? 1 : 0) + 1;

    return (
      <>
        <Toaster>
          {this.error ? (
            <Toast
              message={this.error}
              intent={Intent.DANGER}
              onDismiss={() => (this.error = "")}
            />
          ) : null}
        </Toaster>

        {this.props.schema_store!.dialog}

        {this.loading ? null : (
          <div
            style={{
              padding: 20,
              width: "100%",
              height: "100%",
              paddingBottom: CONTROL_HEIGHT
            }}
          >
            <AutoSizer>
              {({ height, width }) => (
                <MultiGrid
                  cellRenderer={props => (
                    <CellRenderer
                      {...props}
                      schema_store={this.props.schema_store}
                      elements={this.elements}
                      keys={this.keys}
                      fix_column_count={fix_column_count}
                      onClick={this.onClick}
                    />
                  )}
                  columnCount={this.keys.length + fix_column_count}
                  fixedColumnCount={fix_column_count}
                  fixedRowCount={1}
                  height={height}
                  rowHeight={45}
                  rowCount={this.elements.length + 1}
                  width={width}
                  columnWidth={cache.columnWidth}
                  deferredMeasurementCache={cache}
                />
              )}
            </AutoSizer>
          </div>
        )}
        <div
          style={{
            position: "absolute",
            bottom: 0,
            right: 0,
            width: "100%",
            height: CONTROL_HEIGHT
          }}
        >
          <Navbar.Group align={Alignment.RIGHT}>
            <Navbar.Divider />
            <DrawerButton
              minimal
              schema_store={this.props.schema_store}
              content={localize("ls-filterTooltip")}
              icon={"filter"}
              text={localize("ls-filterButton")}
              keys={this.keys}
            />
            {can_export ? (
              <PromisButton
                minimal
                icon="export"
                text={localize("ls-Export")}
                onClick={this.export}
              />
            ) : null}
            {can_import ? <ImportDialogButton {...this.props} /> : null}
            {can_add ? createAddButton() : null}
          </Navbar.Group>
        </div>
      </>
    );
  }
}
