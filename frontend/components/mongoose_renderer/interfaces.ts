import { LocalizedStrings } from "localized-strings";
import { AxiosResponse, AxiosError } from "axios";
import { AccessRights } from "../../typescript/typedefs";
import { USER_RIGHTS } from "../../../common/access_levels";

export enum EFiltter {
  GRATER,
  SMALLER,
  GRATER_OR_EQUAL,
  SMALLER_OR_EQUAL,
  REGEX
}

export type IMTypes =
  | String
  | Number
  | Date
  | Boolean
  | Array<any>
  | Map<any, any>;
export interface MShemaElement<T> {
  type: IMTypes;
  required: boolean;
  renderer?: {
    label?: string; // if localizator is used than this iss a key
    help?: string; // if localizator is used than this iss a key
    tooltip?: string; // if localizator is used than this iss a key
    email_field?: boolean;
    password_field?: boolean;
    is_file?: boolean;
    key?: Function;
    ref_tabe?: string;
  };
  enum?: T[];
  default?: T;
}

export interface MBataBaseElement {
  _id: string;
  [key: string]: any;
}

export interface IUser {
  _id?: string;
  email: string;
  name?: string;
  priority?: AccessRights;
  avatar?: string;
}

export interface MSchema {
  [keymin_lengthmin_length: string]: MShemaElement<any>;
}

export interface MTable {
  name: string;
  schema: MSchema;
  rights: {
    post: USER_RIGHTS;
    get: USER_RIGHTS;
    update: USER_RIGHTS;
    delete: USER_RIGHTS;
    export: USER_RIGHTS;
    import: USER_RIGHTS;
  };
}

export interface ISchemarenderer {
  schema: MSchema;
  post_address?: string;
  delete_adress?: string;
  get_all_address?: string;
  localizator?: LocalizedStrings<any>;
}

interface BaseAxioMongoose {
  address: string;
  config?: Object;
  fail_function?: Function;
  success_function?: Function;
}

export interface PostFormAxioMongoose extends BaseAxioMongoose {
  object_as_config?: boolean;
  /**Flag to determin if server provides a error responce in form of
   * post object, if yes then server should return an object with errors mimicing object which was send
   * Then errors is displayed for each field separetly */

  fail_contain_object_error?: boolean;
  fail_function?: {
    (response: AxiosError, object?: Object): void;
  };
  success_function?: {
    (response: AxiosResponse, object?: Object): void;
  };
}

export type IRefClickFunction = {
  (
    ref: { [key: string]: string },
    schema_object: MShemaElement<any>,
    field?: string
  ): void;
};

export interface IBaseMongoose {
  keys?: string[];
  exclude_keys?: string[];
  disabled_keys?: string[];
  fetch_url?: BaseAxioMongoose;
  post_url?: BaseAxioMongoose;
  on_ref_click?: IRefClickFunction;
}
