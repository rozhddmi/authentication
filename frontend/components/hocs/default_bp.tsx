import * as React from "react";
import { inject, observer } from "mobx-react";
import { AppStore } from "../../stores/app_store";
import { PossibleTab } from "../../typescript/typedefs";
import { MainContainer } from "../../stores";

export const withPermission = (
  Component: any,
  table: PossibleTab,
  action: string
) => {
  const ComponentParent = observer((props: { appStore: AppStore }, {}) => {
    const hasPermission = MainContainer.appStore.canAccessTable(table, action);
    return hasPermission ? <Component {...props} /> : null;
  });

  return ComponentParent;
};
