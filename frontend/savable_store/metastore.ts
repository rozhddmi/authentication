import Dexie from "dexie";
import "reflect-metadata";
import { observable, observe } from "mobx";

interface IHistory {
  object: Object;
  property_name: string;
  oldValue: any;
  newValue: any;
  type?: string;
}

interface ISavable {
  class_plus_prop_name: string;
  property: string;
  class_name: string;
  value: any;
}

export class DexieStore extends Dexie {
  static OBSERVER = "__dbObserver";
  static CLASSNAMEFIELD = "__dbName";
  static CONSTRUCTING_META = "__dbConstructingFinished";

  static SAVABLE_PROPERTIES = "__dbSavables";

  //   private history: IHistory[] = [];
  //   private history_position = -1;
  private savable_table: Dexie.Table<ISavable, string>;
  constructor() {
    super("Savables");
    this.version(1).stores({
      savable_table: "++class_plus_prop_name, property, class_name"
    });
    this.savable_table = this.table("savable_table");
  }

  DBClassDecorator = <T extends { new (...constructorArgs: any[]) }>(
    constructorFunction: T
  ) => {
    //new constructor function
    const loadObject = this.loadObject;
    const handleChange = this.handleChange;
    let newConstructorFunction: any = function(...args) {
      let func: any = function() {
        return new constructorFunction(...args);
      };
      func.prototype = constructorFunction.prototype;
      let result: any = new func();
      DexieStore.addClassNameMeta(result);
      DexieStore.getSavableProperties(result).forEach(([property_name, tp]) => {
        if (tp === Array) {
          observe(result[property_name], e => {
            handleChange({
              object: result,
              newValue: JSON.parse(JSON.stringify(e.object)),
              property_name,
              oldValue: undefined
            });
          });
        } else {
          observe(result as any, property_name, e => {
            handleChange({
              object: result,
              newValue: e.newValue,
              property_name,
              oldValue: e.oldValue
            });
          });
        }
      });
      loadObject(result);

      DexieStore.setCunstructionFinished(result);
      return result;
    };
    newConstructorFunction.prototype = constructorFunction.prototype;
    return newConstructorFunction;
  };

  DBClassVariableDecorator = (name: string) => {
    const that = this;
    return function<T extends Object>(obj: T): T {
      DexieStore.addClassNameMeta(obj, name);
      that.loadObject(obj);
      return obj;
    };
  };

  DBPropertyDecoartor = (target: Object, propertyName: string) => {
    // property value
    var classConstructor = target.constructor;
    const metadata = DexieStore.getSavableProperties(target);
    const tp = Reflect.getMetadata("design:type", target, propertyName);
    metadata.push([propertyName, tp]);
    Reflect.defineMetadata(
      DexieStore.SAVABLE_PROPERTIES,
      metadata,
      classConstructor
    );
    return observable(target, propertyName);
  };

  static addClassNameMeta(object: Object, name?: string) {
    const name_field = name || object.constructor.name;
    Reflect.defineMetadata(DexieStore.CLASSNAMEFIELD, name_field, object);
  }

  static setCunstructionFinished = (constructorFunction: Function | Object) => {
    Reflect.defineMetadata(
      DexieStore.CONSTRUCTING_META,
      true,
      constructorFunction
    );
  };

  static getSavableProperties = (obj: Object): [string, any][] => {
    var classConstructor = obj.constructor;
    return (
      Reflect.getMetadata(DexieStore.SAVABLE_PROPERTIES, classConstructor) || []
    );
  };

  static getSavableName(obj: Object): string | undefined {
    var classConstructor = obj;
    return Reflect.getMetadata(DexieStore.CLASSNAMEFIELD, classConstructor);
  }

  private handleChange = (change: IHistory) => {
    const class_name = Reflect.getMetadata(
      DexieStore.CLASSNAMEFIELD,
      change.object
    );
    const class_constructing_finished = Reflect.getMetadata(
      DexieStore.CONSTRUCTING_META,
      change.object
    );

    if (
      class_name &&
      class_constructing_finished &&
      change.oldValue !== change.newValue
    ) {
      this.savable_table.put({
        class_plus_prop_name: `${class_name}.${change.property_name}`, // Primarry key
        property: change.property_name,
        class_name,
        value: change.newValue
      });
    }
    //this.history.push(change);
  };

  loadObject = (object: Object) => {
    const name = DexieStore.getSavableName(object);
    if (!name) {
      return;
    }
    DexieStore.getSavableProperties(object).forEach(([element, tp]) => {
      this.savable_table.get(`${name}.${element}`, res => {
        if (res) {
          if (tp === Array && object[element] !== undefined) {
            object[element].length = 0;
            object[element].push(...res.value);
          } else {
            object[element] = res.value;
          }
        }
      });
    });
  };
}
