import { DexieStore } from "./metastore";

const DEXIE_LOADER = new DexieStore();

export const SavableClass = DEXIE_LOADER.DBClassDecorator;

export const SavableObservable = DEXIE_LOADER.DBPropertyDecoartor;

export const RenameSavable = DEXIE_LOADER.DBClassVariableDecorator;
