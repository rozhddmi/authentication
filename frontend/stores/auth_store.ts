import { observable, action, autorun } from "mobx";
import { StoreContainer } from "./store_container";
import axios from "axios";
import jwt_decode from "jwt-decode";
import { checkRights } from "../../common/functions";
import { ShemaRenderer, IUser } from "../components/index";
import { Tables } from "../../common/schemas/schemas";
import { RenameSavable } from "../savable_store/decorator_factory";

const LOGIN_ADDRESS = "/api/users/login";
const REGISTER_ADDRESS = "/api/post/Users";
const GET_ADDRESS = "/api/get/Users";
const DELETE_ADDRESS = "/api/delete/Users";
const UPDATE_ADDRESS = "/api/update/Users";

export class AuthentificationStore {
  private stores: StoreContainer;
  @observable is_authenticated: boolean = false;
  @observable form_dialog: JSX.Element | undefined;
  @observable user: IUser | undefined = undefined;

  readonly authentication_path: string = "/login";

  private user_shema: ShemaRenderer;

  constructor(stores: StoreContainer) {
    this.stores = stores;
    this.user_shema = RenameSavable("UserSchema")(
      new ShemaRenderer(
        Tables.Users,
        this.user,
        this.stores.appStore.localizator,
        {
          get: GET_ADDRESS,
          post: REGISTER_ADDRESS,
          update: UPDATE_ADDRESS,
          delete: DELETE_ADDRESS
        }
      )
    );
    autorun(() => {
      this.user_shema.setNewUser(this.user);
    });
  }

  @action
  setFormDialog(child?: JSX.Element) {
    this.form_dialog = child;
  }

  loginButton(oppened?: boolean) {
    return this.user_shema.createFormButtonDialog({
      dialog_button_icon: "log-in",
      dialog_button_tooltip: "ls-logIn",
      button_text: "ls-logIn",
      form_lable: "ls-loginForm",
      form_footer: "ls-contactManager",
      keys: ["email", "password"],
      post_url: {
        address: LOGIN_ADDRESS,
        success_function: res => this.setAuthToken(res.data.token),
        object_as_config: true,
        fail_contain_object_error: true
      },
      method: "post"
    });
  }

  registerUserButton(oppened?: boolean, show_text?: boolean) {
    return this.user_shema.createFormButtonDialog({
      dialog_button_icon: "add",
      dialog_button_tooltip: "registerUser",
      button_text: "register",
      form_lable: "registrationForm",
      exclude_keys: ["date", "avatar"],
      method: "post",
      post_url: {
        address: REGISTER_ADDRESS,
        success_function: (res, obj: any) => {
          this.stores.showToastMessage(
            "User {0} registerd with {1} rights".format(
              obj["name"],
              obj["priority"]
            )
          );
        },
        object_as_config: true,
        fail_contain_object_error: true
      }
    });
  }

  get userTable() {
    return this.user_shema.createTableView({
      fetch_url: { address: GET_ADDRESS },
      post_url: {
        address: UPDATE_ADDRESS,
        success_function: e => {
          this.stores.showToastMessage("User sucessfully edited");
        },
        object_as_config: true,
        fail_contain_object_error: true
      },
      on_ref_click: (obj, elem_obj, field) => {
        console.log("ref_clicked");
        if (field === "last_update") {
          const table = Tables[elem_obj.renderer!.ref_tabe];
          const dialog = this.stores.historyStore.schema.createDialog({
            obj,
            isOpen: true,
            closeDialog: () => this.setFormDialog(),
            onClose: () => this.setFormDialog(),
            method: "update",
            disabled_keys: Object.keys(table.schema).filter(key => {
              return !checkRights(table, this.user, "update", key);
            })
          });
          this.setFormDialog(dialog);
        }
      }
    });
  }

  @action
  setAuthToken = (token, gotodashboard = true) => {
    if (token) {
      // Apply authorization token to every request if logged in
      // Set token to localStorage
      localStorage.setItem("jwtToken", token);
      // Set token to Auth header
      axios.defaults.headers.common["Authorization"] = token;
      // Decode token to get user data
      const decoded = jwt_decode<IUser>(token);
      // Set current user
      this.setCurrentUser(decoded);
      this.is_authenticated = true;
      if (gotodashboard) this.goToDashboard();
      const currentTime = Date.now() / 1000; // to get in milliseconds
      if ((decoded as any).exp < currentTime) {
        // Logout user
        this.logoutUser();
        // Redirect to login
        this.goToLogIn();
      }
    } else {
      // Remove token from local storage
      localStorage.removeItem("jwtToken");
      // Delete auth header
      delete axios.defaults.headers.common["Authorization"];
      this.is_authenticated = false;
      this.setCurrentUser(undefined);
      this.goToLogIn();
    }
    this.goToDashboard();
    //his.updateUsersMap();
  };

  private __historyPush(address: string) {
    log("historry pushed", address);
    this.stores.history.push(address);
  }

  goToLogIn = () => {
    this.__historyPush("/login");
  };

  goToDashboard = () => {
    this.__historyPush("/dashboard");
  };

  goToRegister = () => {
    this.__historyPush("/register");
  };

  goHome = () => {
    this.__historyPush("/");
  };

  // Set logged in user
  @action
  setCurrentUser = (user: IUser | undefined) => {
    this.user = user;
  };

  // Log user out
  @action
  logoutUser = () => {
    this.setAuthToken(false);
  };
}
