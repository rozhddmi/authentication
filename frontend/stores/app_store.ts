import { observable, action, computed } from "mobx";
import { StoreContainer } from "./store_container";
import { checkRights } from "../../common/functions";
import { Tables } from "../../common/schemas/schemas";

import LocalizedStrings from "localized-strings";
import LOCALIZED_STRING from "../../common/localization";
import { ILangs, PossibleTab } from "../typescript/typedefs";
import { IUser } from "../components";
import {
  SavableObservable,
  SavableClass
} from "../savable_store/decorator_factory";

@SavableClass
export class AppStore {
  localizator = new LocalizedStrings(LOCALIZED_STRING);

  readonly possible_tabs: Map<string, PossibleTab> = new Map([
    ["Users", <PossibleTab>"Users"],
    ["Vehicles", <PossibleTab>"Vehicles"],
    ["Customers", <PossibleTab>"Customers"],
    ["Documents", <PossibleTab>"Documents"],
    ["Templates", <PossibleTab>"Templates"],
    ["History", <PossibleTab>"History"]
  ]);

  @SavableObservable curent_laguage: ILangs = "en";
  @SavableObservable curent_tab: PossibleTab = "Users";

  @observable selected_user: IUser | undefined;
  @observable user_edit_opened: boolean;
  @observable user_delete_opened: boolean;

  @observable selected_document: any;
  @observable document_edit_opened: boolean;

  @observable selected_template: any;
  @observable template_edit_opened: boolean;

  @observable selected_person: any;
  @observable person_edit_opened: boolean;

  @observable selected_car: any;
  @observable car_edit_opened: boolean;

  private stores: StoreContainer;

  constructor(stores: StoreContainer) {
    console.log("origin constructor is calles");
    this.stores = stores;
  }

  @action.bound
  setCurrentLang(lng: ILangs) {
    this.curent_laguage = lng;
  }

  localize = (field: string): string => {
    return (
      this.localizator.getString(field as string, undefined, true) || field
    );
  };

  /**
   * Set tab  oppened in dashboard window
   * observed in navigationba and dashboard
   * @param tab
   */
  @action.bound
  setCurrentTab(tab: PossibleTab) {
    this.curent_tab = tab;
  }

  /**
   * set selected user for editing if undefined then registration window is oppened
   * observed in UserEditDialog.tsx
   * @param user IUser or undefined
   */
  @action
  setSelectedUser(user?: IUser) {
    this.selected_user = user;
  }

  /**
   * Open a user edit register dialog
   * observed in dialog/edituser.tsx
   * @param flag true or false
   */
  @action
  setUserEditDialog(user: IUser | undefined, action: "edit" | "delete") {
    if (action === "edit") {
      this.user_edit_opened = user !== undefined;
    } else {
      this.user_delete_opened = user !== undefined;
    }
  }

  @computed
  get current_user(): undefined | IUser {
    return this.stores.registerStore!.user;
  }

  @computed
  get avaliable_tabs(): PossibleTab[] {
    return Array.from(this.possible_tabs.values()).filter(id =>
      this.canAccessTable(id)
    );
  }

  canAccessTable = (table: PossibleTab, action = "get"): boolean => {
    return checkRights(Tables[table], this.current_user, action);
  };
}
