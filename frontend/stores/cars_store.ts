import { observable, action, autorun, computed } from "mobx";
import { StoreContainer } from "./store_container";
import { checkRights } from "../../common/functions";
import { ShemaRenderer } from "../components/index";
import { Tables } from "../../common/schemas/schemas";

const GET_ADDRESS = "/api/get/Vehicles";
const UPDATE_ADDRESS = "/api/update/Vehicles";
const POST_ADDRESS = "/api/post/Vehicles";
const DELETE_ADDRESS = "/api/delete/Vehicles";

export class CarStore {
  private stores: StoreContainer;
  @observable form_dialog: JSX.Element | undefined;
  schema: ShemaRenderer;

  constructor(stores: StoreContainer) {
    this.stores = stores;
    autorun(() => {
      const { user } = this.stores.registerStore;
      this.schema = new ShemaRenderer(
        Tables.Vehicles,
        user,
        this.stores.appStore.localizator,
        {
          get: GET_ADDRESS,
          post: POST_ADDRESS,
          update: UPDATE_ADDRESS,
          delete: DELETE_ADDRESS
        }
      );
    });
  }
  @computed
  get user() {
    return this.stores.registerStore.user;
  }

  @action
  setFormDialog(child?: JSX.Element) {
    this.form_dialog = child;
  }

  get carTable() {
    return this.schema.createTableView({
      fetch_url: { address: GET_ADDRESS },
      post_url: {
        address: UPDATE_ADDRESS,
        success_function: e => {
          this.stores.showToastMessage("User sucessfully edited");
        },
        object_as_config: true,
        fail_contain_object_error: true
      },
      on_ref_click: (obj, elem_obj, field) => {
        console.log("ref_clicked");
        if (field === "last_update") {
          const table = Tables[elem_obj.renderer!.ref_tabe];
          const dialog = this.stores.historyStore.schema.createDialog({
            obj,
            isOpen: true,
            closeDialog: () => this.setFormDialog(),
            onClose: () => this.setFormDialog(),
            method: "update",
            disabled_keys: Object.keys(table.schema).filter(key => {
              return !checkRights(table, this.user, "update", key);
            })
          });
          this.setFormDialog(dialog);
        }
      }
    });
  }
}
