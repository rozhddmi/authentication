import { AuthentificationStore } from "./auth_store";
import { CarStore } from "./cars_store";

import { createBrowserHistory, History } from "history";
import { Position, Toaster, Intent } from "@blueprintjs/core";
import { AppStore } from "./app_store";
import { HistoryStore } from "./history_store";

/** Singleton toaster instance. Create separate instances for different options. */

export class StoreContainer {
  readonly history: History<any> = createBrowserHistory();
  readonly appStore: AppStore = new AppStore(this);

  readonly registerStore: AuthentificationStore = new AuthentificationStore(
    this
  );
  readonly historyStore = new HistoryStore(this);
  readonly carsStore: CarStore = new CarStore(this);

  private appToaster = Toaster.create({
    className: "recipe-toaster",
    position: Position.TOP
  });

  showToastMessage = (msg: string, intent: Intent = Intent.PRIMARY) => {
    this.appToaster.show({ message: msg, intent: intent });
  };
}

export const MainContainer = new StoreContainer();
