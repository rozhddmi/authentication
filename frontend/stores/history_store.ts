import { observable, action, autorun } from "mobx";
import { StoreContainer } from "./store_container";
import { checkRights } from "../../common/functions";
import { ShemaRenderer } from "../components/index";
import { Tables } from "../../common/schemas/schemas";

const GET_ADDRESS = "/api/get/History";

export class HistoryStore {
  private stores: StoreContainer;
  @observable form_dialog: JSX.Element | undefined;
  schema: ShemaRenderer;

  constructor(stores: StoreContainer) {
    this.stores = stores;
    autorun(() => {
      const { user } = this.stores.registerStore;
      this.schema = new ShemaRenderer(
        Tables.History,
        user,
        this.stores.appStore.localizator,
        {
          get: GET_ADDRESS
        }
      );
    });
  }

  @action
  setFormDialog(child?: JSX.Element) {
    this.form_dialog = child;
  }

  historyTable = () => {
    return this.schema.createTableView({
      fetch_url: { address: GET_ADDRESS },

      on_ref_click: (obj, elem_obj, field) => {
        console.log("ref_clicked");
        if (field === "last_update") {
          const table = Tables[elem_obj.renderer!.ref_tabe];
          const dialog = this.schema.createDialog({
            obj,
            keys: Object.keys(table.schema),
            isOpen: true,
            closeDialog: () => this.setFormDialog(),
            onClose: () => this.setFormDialog(),
            method: "update",
            disabled_keys: Object.keys(table.schema).filter(key => {
              return !checkRights(
                table,
                this.stores.registerStore.user,
                "update",
                key
              );
            })
          });
          this.setFormDialog(dialog);
        }
      }
    });
  };
}
