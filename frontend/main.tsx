import("react-hot-loader");
import React from "react";
import * as ReactDOM from "react-dom";

// --- Import global assets so webpack includes them
import "./app.global.css";
import "./assets/company_logo.png";
import "./typescript/extra_prototypes"; // All prototypes are defined there

import App from "./components/app";

// disable warnings

log(VERSION, PRODUCTION);

const render = Component => {
  log("Rendering");
  ReactDOM.render(<Component />, document.getElementById("root"));
};

render(App);
