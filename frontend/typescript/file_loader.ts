export enum LMode {
  BUFFERED,
  WAITING
}

class MyFileReader extends FileReader {
  public size: number;
  public offset: number;
  public index: number;
}

interface ResInterface {
  size?: number;
  offset?: number;
  result?: any;
}

type CAL_PRG = { (data: any, progress: number): void };
type CAL_DONE = { (data?: any): void };

export class BigFileLoader {
  public mode: LMode;
  public chunkSize: number = 1024 * 1024; // bytes
  private lastOffset: number = 0;
  private chunkReorder: number = 0;
  private chunkTotal: number = 0;
  private timeout: number = 10; // milliseconds
  private previous: ResInterface[] = [];
  public onprogress: CAL_PRG;
  public onload: CAL_DONE;
  public onloadstart: { (ev?: Event): void };
  public onerror: { (ev: Event): void };
  private data: any[] | string;
  private counter: number = 0;
  private fileSize: number = 0;

  constructor(mode: LMode = LMode.BUFFERED) {
    this.mode = mode;
  }

  private _callbackProgress = data => {
    // TODO calculate percentage of loaded file
    this.counter += data.byteLength | data.length;
    this.data = (this.data as any).concat(data);
    if (this.onprogress)
      this.onprogress(this.data, this.counter / this.fileSize);
  };

  private _callbackFinal = () => {
    if (this.onprogress) this.onprogress(this.data, 1.0);
    if (this.onload) this.onload(this.data);
  };

  public readAsText(file: File) {
    // var chunkSize  = 1024*1024; // bytes
    this.data = "";
    this.init(file);
    if (this.onloadstart) this.onloadstart();
    let offset = 0;
    const size = this.chunkSize;
    let partial;
    let index = 0;
    if (file.size === 0) {
      this._callbackFinal();
    }
    const callback = this.callbackRead;

    while (offset < file.size) {
      partial = file.slice(offset, offset + size);
      const reader = new MyFileReader();
      reader.size = this.chunkSize;
      reader.offset = offset;
      reader.index = index;
      reader.onload = function(evt) {
        callback(this as MyFileReader, file, evt);
      };
      reader.onerror = this.onerror;
      reader.readAsText(partial);
      offset += this.chunkSize;
      index += 1;
    }
  }

  private callbackRead = (obj: MyFileReader, file: File, evt) => {
    if (this.mode === LMode.BUFFERED) {
      this.callbackRead_buffered(obj, file, evt);
    } else {
      this.callbackRead_waiting(obj, file, evt);
    }
  };

  private callbackRead_waiting = (reader: MyFileReader, file: File, evt) => {
    if (this.lastOffset === reader.offset) {
      // console.log("[", reader.size, "]", reader.offset, '->', reader.offset + reader.size, "");
      this.lastOffset = reader.offset + reader.size;
      this._callbackProgress(evt.target.result);
      if (reader.offset + reader.size >= file.size) {
        this.lastOffset = 0;
        this._callbackFinal();
      }
      this.chunkTotal++;
    } else {
      // console.log("[", reader.size, "]", reader.offset, '->', reader.offset + reader.size, "wait");
      setTimeout(function() {
        this.callbackRead_waiting(reader, file, evt);
      }, this.timeout);
      this.chunkReorder = this.chunkReorder + 1;
    }
  };

  private callbackRead_buffered = (reader: MyFileReader, file, evt) => {
    this.chunkTotal = this.chunkTotal + 1;
    if (this.lastOffset !== reader.offset) {
      // out of order
      // console.log("[", reader.size, "]", reader.offset, '->', reader.offset + reader.size, ">>buffer");
      this.previous.push({
        offset: reader.offset,
        size: reader.size,
        result: reader.result
      });
      this.chunkReorder = this.chunkReorder + 1;
      return;
    }

    const parseResult = (offset, size, result) => {
      this.lastOffset = offset + size;
      this._callbackProgress(result);
      if (offset + size >= file.size) {
        this.lastOffset = 0;
        this._callbackFinal();
      }
    };

    // in order
    // console.log("[", reader.size, "]", reader.offset, '->', reader.offset + reader.size, "");
    parseResult(reader.offset, reader.size, reader.result);

    // resolve previous buffered
    let buffered: ResInterface[] = [{}];
    while (buffered.length > 0) {
      buffered = this.previous.filter(item => item.offset === this.lastOffset);
      buffered.forEach(item => {
        // console.log("[", item.size, "]", item.offset, '->', item.offset + item.size, "<<buffer");
        parseResult(item.offset, item.size, item.result);
        this.previous.remove(item);
      });
    }
  };
  public init(file?: File) {
    this.lastOffset = 0;
    this.chunkReorder = 0;
    this.chunkTotal = 0;
    this.previous = [];
    this.counter = 0;
    this.fileSize = file ? file.size : 0;
  }
}
