export declare var PRODUCTION: boolean;
export declare var VERSION: string;

export type ILangs = "en" | "cz" | "rus";
export const Langs = ["en", "cz", "rus"];

export type PossibleTab =
  | "Users"
  | "Vehicles"
  | "Customers"
  | "Documents"
  | "Templates"
  | "Home"
  | "History";

export type AccessRights = "user" | "manager" | "admin" | "";

export type DummyType = string;
