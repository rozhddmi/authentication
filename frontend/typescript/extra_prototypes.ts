declare global {
  interface Array<T> {
    last(): T;
    remove(i: T): void;
    flat(depth?: number): void;
  }
  interface String {
    format(...args: any[]);
    endsWith(searchString: string, position?: number): boolean;
    replaceAll(search: string, replacement: string): string;
  }
  interface ArrayBuffer {
    concat(a: ArrayBuffer): ArrayBuffer;
  }

  var VERSION: string;
  var PRODUCTION: string;
  var log;
}

(global as any).log = (...args: any): void => {
  if (!PRODUCTION) {
    //console.log(...args);
  }
};

// ----------- String endsWith ------------------------------
String.prototype.format =
  String.prototype.format ||
  function(...args: any[]) {
    const fmt = this;
    if (!fmt.match(/^(?:(?:(?:[^{}]|(?:\{\{)|(?:\}\}))+)|(?:\{[0-9]+\}))+$/)) {
      throw new Error("invalid format string.");
    }
    return fmt.replace(
      /((?:[^{}]|(?:\{\{)|(?:\}\}))+)|(?:\{([0-9]+)\})/g,
      (m, str, index) => {
        if (str) {
          return str.replace(/(?:{{)|(?:}})/g, m => m[0]);
        } else {
          if (index >= args.length) {
            throw new Error("argument index is out of range in format");
          }
          return args[index];
        }
      }
    );
  };

String.prototype.replaceAll =
  String.prototype.replaceAll ||
  function(search: string, replacement: string) {
    const target = this;
    return target.split(search).join(replacement);
  };

String.prototype.endsWith =
  String.prototype.endsWith ||
  function(searchString: string, position?: number): boolean {
    const subjectString = this.toString();
    if (
      typeof position !== "number" ||
      !isFinite(position) ||
      Math.floor(position) !== position ||
      position > subjectString.length
    ) {
      position = subjectString.length;
    }
    position! -= searchString.length;
    const lastIndex = subjectString.lastIndexOf(searchString, position);
    return lastIndex !== -1 && lastIndex === position;
  };

// ---------- Array prototype to get last element quicker -------------
Array.prototype.last =
  Array.prototype.last ||
  function(): any {
    return this[this.length - 1];
  };

Array.prototype.remove =
  Array.prototype.remove ||
  function(val) {
    let i = this.length;
    while (i--) {
      if (this[i] === val) {
        this.splice(i, 1);
      }
    }
  };

Array.prototype.flat =
  Array.prototype.flat ||
  function() {
    let depth = arguments[0];
    depth = depth === undefined ? 1 : Math.floor(depth);
    if (depth < 1) return Array.prototype.slice.call(this);
    return (function flat(arr, depth) {
      const len = arr.length >>> 0;
      let flattened = [];
      let i = 0;
      while (i < len) {
        if (i in arr) {
          const el = arr[i];
          if (Array.isArray(el) && depth > 0)
            flattened = flattened.concat(flat(el, depth - 1));
          else flattened.push(el as never);
        }
        i++;
      }
      return flattened;
    })(this, depth);
  };

export {};
