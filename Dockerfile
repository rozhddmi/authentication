FROM node:jessie


WORKDIR /car_store

COPY ./package.json /car_store
COPY ./tsconfig.json /car_store
COPY ./yarn.lock /car_store
COPY ./webpack.config.prod.js /car_store
RUN mkdir -p dist
RUN yarn
# Remove login credentials

# Build the js files
# disabling cache increase an integer for rebuild
COPY ./frontend /car_store/frontend
COPY ./common /car_store/common

RUN yarn build

RUN echo 'test5' > test && rm test

ARG PORT=5000
ENV PORT=$PORT

COPY ./backend /car_store/dist/backend
COPY ./common /car_store/dist/common

WORKDIR /car_store/dist


EXPOSE ${PORT}
CMD ["node", "backend/server.js"]
