const path = require("path");
const webpack = require("webpack");

const $$__PROD__$$ = process.argv.indexOf("-p") >= 0;
console.log("IS PRODUCTION", $$__PROD__$$);

const SRC = path.join(__dirname, "./frontend");
const DST = path.join(__dirname, "./dist/app");
console.log("Building to ", DST);

// Prepares the index.html file by injecting script tags into specified html file
const HtmlWebpackPlugin = require("html-webpack-plugin");
const htmlPluginInstance = new HtmlWebpackPlugin({
  template: "assets/index.html"
});

// --- Production only plugins

// Minifies JS files
const TerserPlugin = require("terser-webpack-plugin");
// Extracts imported CSS files to files (replaces ExtractTextPlugin)
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
// Minifies and optimizes CSS files
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
// Remove extraneous files (e.g. output from the last build) from the output folder
const WebpackCleanupPlugin = require("webpack-cleanup-plugin");
// Adds a gzipped version of chosen files to the output folder
const CompressionPlugin = require("compression-webpack-plugin");

module.exports = {
  context: SRC,

  entry: { main: "./main.tsx" },

  // output is only used by production mode
  output: {
    path: DST,
    filename: "bundle.js",
    chunkFilename: "[chunkhash].js",
    // Allow for using a cdn to deliver assets, default to / if not set
    publicPath: "" //process.env.CDN_URL || '/'
  },

  resolve: {
    // Extensions here can then be omitted in import/require statements
    extensions: [".js", ".ts", ".tsx"],
    // Fix webpack's default behavior to not load packages with jsnext:main module
    mainFields: ["module", "browser", "main"],
    // Make sure we always use the same single version of mobx
    alias: { mobx: path.resolve(__dirname, "node_modules/mobx") }
  },

  module: {
    rules: [
      // For dev override tsconfig to allow unused locals (makes refactoring easier)
      {
        test: /\.tsx?$/,
        use: {
          loader: "ts-loader",
          options: { compilerOptions: { noUnusedLocals: $$__PROD__$$ } }
        }
      },
      // For production extract css to files, so they can load in parallel to js
      {
        test: /\.css$/,
        use: [
          $$__PROD__$$ ? MiniCssExtractPlugin.loader : "style-loader",
          "css-loader"
        ]
      },
      {
        test: /\.s(a|c)ss$/,
        use: [
          $$__PROD__$$ ? MiniCssExtractPlugin.loader : "style-loader",
          "css-loader",
          "sass-loader"
        ]
        // loader: [
        //     $$__PROD__$$ ? MiniCssExtractPlugin.loader : 'style-loader',
        //     {
        //         loader: 'sass-loader',
        //         options: {
        //             sourceMap: !$$__PROD__$$
        //         }
        //     }
        // ]
      },
      // Keep original filename of jpgs, png and gif images
      { test: /\.(jpe?g|png|gif)$/, loader: "file-loader?name=[name].[ext]" },
      // Add a hash to fonts and svgs (which are probably fonts too)
      {
        test: /\.(woff2?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        loader: "file-loader?name=[name].[ext]"
      }
      // If any other types of files need to be included in the bundle, add them below.
      // Probably use file-loader with name=[name]_[hash].[ext]
      // Make sure to actually import/require the files from somewhere in the code!
    ]
  },

  // No optimization needed for dev
  optimization: $$__PROD__$$
    ? {
        splitChunks: {
          name: true,
          cacheGroups: {
            commons: { chunks: "initial", minChunks: 2 },
            vendors: {
              test: /[\\/]node_modules[\\/]/,
              chunks: "all",
              priority: -10
            }
          }
        },
        runtimeChunk: true,
        minimizer: $$__PROD__$$
          ? [
              new TerserPlugin({
                parallel: true,
                terserOptions: {
                  ecma: 6
                }
              }),
              new OptimizeCSSAssetsPlugin({})
            ]
          : undefined
      }
    : {},

  // Production does cleanup, extracts css to files and makes compressed versions of js and css
  plugins: $$__PROD__$$
    ? [
        //new WebpackCleanupPlugin(),
        new MiniCssExtractPlugin({}),
        htmlPluginInstance,
        new CompressionPlugin({ test: /\.(js|css)$/ }) //,
      ]
    : [htmlPluginInstance],

  // Set HMR and add an overlay for compilation errors
  devServer: {
    contentBase: SRC,
    hot: true,
    port: 3000,
    inline: true,
    stats: "minimal",
    clientLogLevel: "warning",
    overlay: true,
    proxy: {
      "/api": "http://localhost:5000",
      "/dashboard": "http://localhost:5000",
      "/login": "http://localhost:5000",
      "/register": "http://localhost:5000"
    },
    watchContentBase: true
  },

  // Make sure there are no source maps in production
  devtool: $$__PROD__$$ ? undefined : "cheap-module-source-map",

  // Polyfills and mocks to run Node.js environment code in non-Node environments
  node: {
    fs: "empty"
  }
};
